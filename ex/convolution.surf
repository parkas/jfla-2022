/*-------- Integer convolutions --------*/
// Language: Surface
// Chacking: ../compiler.exe Surf convolution.surf

// Implicitly sized
let dot_product (u,v:_) = fold (+) (0, map (*) (u,v))
let convolution (k,i:_) = map (dot_product (k,_)) ($window (i))

// Explicitly sized
let window <<k>> (X:['n+k-1]_) [i<'n, j<k] = X [i+j]
let dot_product <<k>> (u,v:_) = fold (+) (0, map (*) <<k>> (u,v))
let convolution <<k>> (k,i:_) = map (dot_product <<k>> (k,_)) (window (i))

// Convolution expressed with |> operator (used prefixed to build composition)
let convolution_1D (i,k) =
  i
  |> $window
  |> map (|> map  (*) (k,_)
          |> fold (+) (0,_))

let convolution_2D (i,k) =
  i
  |> $window
  |> map (|> $transpose
          |> $window
          |> map (|> $transpose
                  |> map  (map  (*)) (k,_)
                  |> fold (fold (+)) (0,_)))

