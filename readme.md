# Size inference formalization and test

This repository contains a light implementation of the type system described
in JFLA-2022 submitted article 'Inférer et vérifier les tailles de tableaux
avec des types polymorphes'.


## Static analyzer building


With OCaml 4.11 or greater, the project can be build with `dune build`
(or `./build`). This will create a file `compiler.exe`, that supports
size and type inference and verification.

This checker has two font-ends:
- Surface language: the higher level language with array syntax
- Core language: the internal language used for inference and checking.

For each declaration, four versions are dumped:
- Input declaration in Core language (after elaboration)
- Type unified declaration (after type inference if any)
- Reffined declaration (after refinment inference if any)
- Size unified declaration (after size inference if any)


## Examples


To run examples: `./bin/compiler Core ./test/size.core`


Each file of the `ex` folder contains its compilation directive.

Top-level declarations may be followed by a special comment
`// SUCC` `// FAIL` to indicate the wether the static analysis
is supposed to succeed or not.


## Misc

File `syntax.el` provides (basic) Emacs color modes for both the
Core language (`core-mode`) (to use in compilation output) and
the Surface language (`surf-mode`)
