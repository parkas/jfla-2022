open Std

type lang =
  | L_core
  | L_lang
  | L_surf

let lang = ref None
let lang_list = [ L_core, "Core"
                ; L_lang, "Lang"
                ; L_surf, "Surf"
                ]

let get_lang arg =
  try lang := Some (fst (List.find snd#.(String.compare arg)#.((=) 0) lang_list))
  with Not_found -> raise (Arg.Bad "Please specify input language")


let infer = ref 0
let infer_types = "types" , 2
let infer_refin = "refine", 3
let infer_sizes = "sizes" , 4
let infer_none  = "none"  , 5

let error i p e l =
  Format.printf "%a:@.@[<v 2>Error: %a@]@." Std.output_location l p e;
  close_in i;
  exit (1)

let syntax_error p () = Format.printf "Syntax: unexpected token"
let lexing_error p () = Format.printf "Syntax: unexpected lexem"

(* Initial environment with builtins *)
let init_env =
  List.fold (map_fst snd)#.(uncurry IMap.add')
    Core.Builtins.builtins IMap.empty

(* Effect reversed application & forwarding *)
let (|-) a f = f a; a

let p_decl vars (d,_) = Format.printf "%a@." (Core.Printer.decl "let" vars) d


(* Single core declaration analysis *)
let process_decl env decl =
  let open Core in

  (* Size/type variable names *)
  let p_vars = Core.Inference.pv (*
    snd (Ast.TVar.ids ()),
    snd (Ast.SVar.ids ())*)
  in

  let infer level inference check decl =
    decl
    |> (if !infer - 1 < snd level then inference env else id)
    |- (if !infer - 1 < snd level then p_decl p_vars else ignore)
    |- (if !infer - 1 > snd level then ignore else check env)
  in

  (* Type and size inference *)
  let (x,t,e),_ =
    decl
    |- NormalFormChecking.check
    |- p_decl p_vars
    |> infer infer_types TypeInference.top_decl   TypeChecking.check
    |> infer infer_refin RefineInference.top_decl RefineChecking.check
    |> infer infer_sizes SizeInference.top_decl   SizeChecking.check
  in

  (* Environment update *)
  IMap.add (fst x) t env, (x,t,e)


let process_decl env (decl,test) =
  let exception Return of (Core.Ast.pat imap * Core.Ast.decl) in
  let n_fail,loc = Option.value ~default:(true,dummy_loc) test in
  Format.printf "@.";
  if not n_fail then Format.printf "//! Expected *****************************@.";
  try raise (Return (process_decl env decl))
  with
  | Return r ->
     if n_fail then r
     else Std.r_error loc "This declaration should not pass static checks"

  | Std.Error ->
     if n_fail then raise Std.Error
     else Format.printf "//! ****************************** Failure@.";
     env,fst decl



(* Core file typing *)
let process_core file =
  let open Syntax in
  let ic = open_in file in
  let lexbuf = Lexer.init file ic in
  try
    lexbuf
    |> Parser.program Lexer.token
    |> NameResolution.implementation
    |> List.map_fold process_decl init_env
    |> ignore
  with
  | Lexer.Lexical_error _ -> error ic lexing_error () (Lexer.position lexbuf)
  | Parser.Error          -> error ic syntax_error () (Lexer.position lexbuf)
  | Std.Error             -> (close_in ic; exit 1)
  | x -> (close_in ic; raise x)





(* Lang file typing *)
let process_lang file =
  let open Lang in
  let ic = open_in file in
  let lexbuf = Lexer.init file ic in
  try
    lexbuf
    |> Parser.program Lexer.token
    |> Elaboration.implementation
    |> Syntax.NameResolution.implementation
    |> List.map_fold process_decl init_env
    |> ignore
  with
  | Lexer.Lexical_error _ -> error ic lexing_error () (Lexer.position lexbuf)
  | Parser.Error          -> error ic syntax_error () (Lexer.position lexbuf)
  | Std.Error             -> (close_in ic; exit 1)
  | x -> (close_in ic; raise x)



(* Surface file typing *)
let process_surf file =
  let open Surface in
  let ic = open_in file in
  let lexbuf = Lexer.init file ic in
  try
    lexbuf
    |> Parser.program Lexer.token
    |> Elaboration.implementation
    |> Syntax.NameResolution.implementation
    |> List.map_fold process_decl init_env
    |> ignore
  with
  | Lexer.Lexical_error _ -> error ic lexing_error () (Lexer.position lexbuf)
  | Parser.Error          -> error ic syntax_error () (Lexer.position lexbuf)
  | Std.Error             -> (close_in ic; exit 1)
  | x -> (close_in ic; raise x)







let process arg =
  match !lang with
  | None -> get_lang arg
  | Some a -> match a with
              | L_core -> process_core arg
              | L_lang -> process_lang arg
              | L_surf -> process_surf arg


(*  if Filename.check_suffix file ".scx"
  then compile file (Filename.chop_suffix file ".scx")
  else
    raise (Arg.Bad ("don't know what to do with " ^ file))*)

let usage_msg =
  "Usage:\n"
  ^(List.fold_left (fun s (_,n) -> s^"  prgm [options] "^n^" <sources>\n") "" lang_list)
  ^"Options:\n"

let speclist =

  [ "-infer-types",
    Arg.Unit (fun () ->
        if !lang <> Some L_core then raise (Arg.Bad "This option is available for Core mode only");
        if !infer <> 0 then raise (Arg.Bad "Multiple inference setting specified");
        infer := snd infer_types),
    "    Start inference at type inference (Core language only)"

  ; "-infer-refine",
    Arg.Unit (fun () ->
        if !lang <> Some L_core then raise (Arg.Bad "This option is available for Core mode only");
        if !infer <> 0 then raise (Arg.Bad "Multiple inference setting specified");
        infer := snd infer_refin),
    "    Start inference at refinement inference (Core language only)"

  ; "-infer-sizes",
    Arg.Unit (fun () ->
        if !lang <> Some L_core then raise (Arg.Bad "This option is available for Core mode only");
        if !infer <> 0 then raise (Arg.Bad "Multiple inference setting specified");
        infer := snd infer_sizes),
    "    Start inference at size inference (Core language only)"

  ; "-infer-none",
    Arg.Unit (fun () ->
        if !lang <> Some L_core then raise (Arg.Bad "This option is available for Core mode only");
        if !infer <> 0 then raise (Arg.Bad "Multiple inference setting specified");
        infer := snd infer_none),
    "    Only type check sources (Core language only)"

(*  "-infer", Arg.Set Trustre_of_ast.infer_clocks,
  " automatically infer clocks";

  "-fullclocks", Arg.Unit (fun () ->
      Trustre_pp.print_fullclocks := true),
  " Print full clocks in declarations";

  "-show-clocks", Arg.Unit (fun () ->
      Trustre_pp.show_annot_clocks := true),
  " Annotate subexpressions with their clocks";

  "-show-types", Arg.Unit (fun () ->
      Trustre_pp.show_annot_types := true),
  " Annotate subexpressions with their types";*)
]

let _ = Arg.parse (Arg.align speclist) process usage_msg


