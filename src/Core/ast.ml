(** -------- Abstract Syntax Tree -------- **)


(**
   Conventions:
   - To make extensions of the language easier, most
     pattern matchings are exhaustive (causing a warning
     if type is extended). The remaining generic cases
     are marked with (*! DFLT typ !*) comments (where
     typ is the names of the type) for easy finding.
 **)

open Std

(* Equation variables *)
module EVar = Vars (struct let shft = 0 let pref = "e_" end)
module ESet = EVar.Set
module EMap = EVar.Map

(* Type variables *)
module TVar = Vars (struct let shft = 0 let pref = "'" end)
module TSet = TVar.Set
module TMap = TVar.Map

(* Size variables *)
module SVar = Vars (struct let shft = 8 let pref = "'" end)
module SSet = SVar.Set
module SMap = SVar.Map



type base_type =
  | T_int
  | T_bool

type cst =
  | C_int of int
  | C_bool of bool

(* Sizes *)
module Size = struct
 include Lib.Polynom.Make (Int) (SVar.I) (SVar)
 type v = SVar.t
end
type eta = Size.t

(* Types *)
type tau =
  | T_var of TVar.t
  | T_base of base_type
  | T_size of eta
  | T_index of eta
  | T_pair  of tau pair
  | T_arrow of tau pair

(* Wrapper to unify sizes and types *)
module Type = struct
  type t = tau
  type v = TVar.t
  let unit i = T_var i
end


(* Constant type *)
let c_tau = function
  | C_int n  -> T_size (Size.scal n)
  | C_bool _ -> T_base T_bool


(* Type / Size choice *)
type ('t,'s) param =
  | Type of 't
  | Size of 's

(* Type patterns *)
type pat =
  | T_mono of tau
  | T_poly of (Type.v, Size.v) param * pat


(* Variables *)
type var = ident loc

(* Expressions *)
type exp = exp' * EVar.t loc
and exp' =
  | E_var of var
  | E_cst of cst
  | E_pair of exp pair
  | E_coer of exp * tau
  | E_app of exp * exp
  | E_abs of (var*tau) * exp
  | E_App of (Type.t, Size.t) param * exp
  | E_Abs of (Type.v, Size.v) param * exp
  | E_fix of decl
  | E_let of decl * exp
  | E_exist of (SVar.t * exp) * exp
  | E_size of eta
  | E_cond of exp * exp pair

(* Inner declarations *)
and decl = var * pat * exp

(* Top level declarations contains:
   - the inner declaration
   - the sets of quoted type and size variables
   - a test directive (FAIL or SUCC comment)
 *)
type top_decl = (decl * (TSet.t * SSet.t)) * bool loc opt


let exp_eid e = snd#.fst e
let exp_loc e = snd#.snd e

let p_vars () = snd (TVar.ids ()), snd (SVar.ids ())



(** Default structure mapping **)
module AstMap = struct
  let tau fn = function
    | T_pair (t,u)  -> T_pair (fn t, fn u)
    | T_arrow (t,u) -> T_arrow (fn t, fn u)
    | T_var _ | T_base _ | T_index _ | T_size _ as t -> t

  let exp fn = function
    | E_coer (e,t) -> E_coer (fn e, t)
    | E_pair (f,s) -> E_pair (fn f, fn s)
    | E_app (f,e) -> E_app (fn f, fn e)
    | E_abs (x,e) -> E_abs (x, fn e)
    | E_App (v,e) -> E_App (v, fn e)
    | E_Abs (v,e) -> E_Abs (v, fn e)
    | E_fix (x,t,d) -> E_fix (x,t,fn d)
    | E_let ((x,t,d),e) -> E_let ((x,t,fn d), fn e)
    | E_exist ((v,s),e) -> E_exist ((v, fn s), fn e)
    | E_cond (c,b) -> E_cond (fn c, Pair.map fn b)
    | E_var _ | E_cst _ | E_size _ as e -> e

end

let subst_decl pat exp subst (x,p,e) = x, pat subst p, exp subst e
let subst_param tau eta subst = function
  | Type t -> Type (tau subst t)
  | Size s -> Size (eta subst s)

let t_subst subst key = TMap.assoc_default Type.unit key subst
let s_subst subst key = SMap.assoc_default Size.unit key subst


(** Type variables mapping **)
(** Case of type abstraction:
      Viewed as a local abstract type:
      if variable substituted, abstraction is dumped
    Case of size quantification in patterns:
      Must be fresh (unmapped) variable
 **)
module MapT = struct
  module Fn = struct
    let of_subst subst = t_subst subst

    let eta fn : eta -> eta = id

    let rec tau fn = function
      | T_var v -> fn v
      | T_base _ | T_size _ | T_index _ | T_pair _ | T_arrow _ as t
        -> AstMap.tau (tau fn) t

    let rec pat fn = function
      | T_mono t -> T_mono (tau fn t)
      | T_poly (Type v, s) -> assert (fn v = T_var v);
                              T_poly (Type v, pat fn s)
      | T_poly (Size v, s) -> T_poly (Size v, pat fn s)

    let rec exp fn =
      map_fst (function
          | E_coer (e,t) -> E_coer (exp fn e, tau fn t)
          | E_abs ((x,t),e) -> E_abs ((x,tau fn t), exp fn e)
          | E_fix d -> E_fix (subst_decl pat exp fn d)
          | E_let (d,e) -> E_let (subst_decl pat exp fn d, exp fn e)
          | E_App (p,e) -> E_App (subst_param tau eta fn p, exp fn e)
          | E_Abs (Type v,e) -> if fn v <> Type.unit v then exp fn e |> fst
                                else E_Abs (Type v, exp fn e)
          | E_Abs (Size v,e) -> E_Abs (Size v, exp fn e)
          | E_var _ | E_cst _ | E_app _ | E_pair _ | E_size _ | E_exist _ | E_cond _ as e
            -> AstMap.exp (exp fn) e)
  end

  let eta = Fn.(of_subst <|> eta)
  let tau = Fn.(of_subst <|> tau)
  let pat = Fn.(of_subst <|> pat)
  let exp = Fn.(of_subst <|> exp)

end

(** Size variables mapping **)
(** Case of size abstraction:
      Viewed as a local abstract type:
      if variable substituted, abstraction is dumped
    Case of size quantification in patterns:
      Must be fresh (unmapped) variable
 **)
module MapS = struct
  module Fn = struct
    let of_subst subst = SMap.fold Size.compose subst

    let eta fn = fn

    let rec tau fn = function
      | T_size s -> T_size (eta fn s)
      | T_index s -> T_index (eta fn s)
      | T_var _ | T_base _ | T_pair _ | T_arrow _ as t
        -> AstMap.tau (tau fn) t

    let rec pat fn = function
      | T_mono t -> T_mono (tau fn t)
      | T_poly (Type v, s) -> T_poly (Type v, pat fn s)
      | T_poly (Size v, s) -> assert (fn (Size.unit v) =@<Size.compare>@ Size.unit v);
                              T_poly (Size v, pat fn s)

    let rec exp fn =
      map_fst (function
          | E_size s -> E_size (eta fn s)
          | E_coer (e,t) -> E_coer (exp fn e,
                                    tau fn t)
          | E_abs ((x,t),e) -> E_abs ((x,tau fn t), exp fn e)
          | E_fix d -> E_fix (subst_decl pat exp fn d)
          | E_let (d,e) -> E_let (subst_decl pat exp fn d, exp fn e)
          | E_App (p,e) -> E_App (subst_param tau eta fn p, exp fn e)
          | E_Abs (Size v,e) -> if fn (Size.unit v) <> Size.unit v then exp fn e |> fst
                                else E_Abs (Size v, exp fn e)
          | E_Abs (Type v,e) -> E_Abs (Type v, exp fn e)
          | E_var _ | E_cst _ | E_app _ | E_pair _ | E_exist _ | E_cond _ as e
            -> AstMap.exp (exp fn) e)

  end

  let eta = Fn.(of_subst <|> eta)
  let tau = Fn.(of_subst <|> tau)
  let pat = Fn.(of_subst <|> pat)
  let exp = Fn.(of_subst <|> exp)

end




(** Type / Size variable extraction from various structures **)
module Vars = struct

  (* Size variables of size *)
  let eta_sizes s = Size.vars s

  (* Size variables of type *)
  let tau_sizes t =
    let rec vars = function
      | T_size s | T_index s -> Size.vars s |> SSet.union
      | T_var _ | T_base _ -> id
      | T_pair (t,u) | T_arrow (t,u) -> vars t <|> vars u
    in vars t SSet.empty

  (* Size variables of type pattern *)
  let pat_sizes p =
    let rec vars = function
      | T_mono t -> tau_sizes t |> SSet.diff
      | T_poly (Size v,p) -> SSet.add v <|> vars p
      | T_poly (Type _,p) -> vars p
    in vars p SSet.empty


  (* Type variables of size *)
  let eta_types (s:eta) = TSet.empty

  (* Type variables of type *)
  let tau_types t =
    let rec vars = function
      | T_var v -> TSet.add v
      | T_base _ | T_size _ | T_index _ -> id
      | T_pair (t,u) | T_arrow (t,u) -> vars t <|> vars u
    in vars t TSet.empty

  (* Type variables of type pattern *)
  let pat_types p =
    let rec vars = function
      | T_mono t -> tau_types t |> TSet.diff
      | T_poly (Type v,p) -> TSet.add v <|> vars p
      | T_poly (Size _,p) -> vars p
    in vars p TSet.empty


end
