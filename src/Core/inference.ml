(** -------- Common inference utils -------- **)


include Std
include Ast


let u_DEBUG = 0
let debug n = (if n < u_DEBUG then Format.fprintf else Format.ifprintf) Format.std_formatter

let _,s = SVar.ids ()
let _,t = TVar.ids ()
let pv = (t,s)
let p_vars = if u_DEBUG = 0 then p_vars else fun () -> pv


(* Some constructor helpers *)
let poly v pat = T_poly (v, pat)
let mono tau = T_mono tau
let (=>) t u = T_arrow (t,u)



(* Extract type and variables of a type scheme *)
let pat_desc pat =
  let rec collect = function
    | T_poly (Type v, p) -> map_fst (TSet.add v) <|> collect p
    | T_poly (Size v, p) -> map_snd (SSet.add v) <|> collect p
    | T_mono t -> fun d -> t,d
  in collect pat (TSet.empty,SSet.empty)
