

open Std
open Ast

let n = Size.unit ~!SVar.create
let k = Size.unit ~!SVar.create
let t = Type.unit ~!TVar.create
let u = Type.unit ~!TVar.create
let v = Type.unit ~!TVar.create
let w = Type.unit ~!TVar.create

let int = T_base T_int
let bool = T_base T_bool


let (=>) = List.fold_right (fun d c -> T_arrow (d,c))
let (^.) n t = [T_index n] => t
let (~:) n = T_size n

let builtins = ref []

let (!) id vars args tau =
  let id = "$"^id in
  let bt =
    (id,Names.create id),
    T_mono (args => tau)
    |> List.fold_right (fun v t ->
           T_poly ((function
                    | Type t -> Type (TSet.choose (Vars.tau_types t))
                    | Size s -> Size (SSet.choose (Vars.eta_sizes s))
                   ) v, t)) vars
  in
  builtins := bt :: !builtins;
  bt

(* Binop builder *)
let (#:) l t a b c =
(*  let p =
    List.fold_right (fun [@warning "-8"] (T_var v) t -> T_poly (Type v,t))
      t (T_mono ([a;b] => c))
  in*)
(*  List.map (fun x -> ! ("$"^x),p) l*)
  List.map (fun x -> !x t [a;b] c) l

(* Polymorphic Comparison *)
let [@warning "-8"] [eq;ne] =
  ["eq";"ne"]
    #: [Type t] t t bool

(* Order relations *)
let [@warning "-8"] [ge;le;gt;lt] =
  ["ge";"le";"gt";"lt"]
    #: [] int int bool

(* Boolean operators *)
let [@warning "-8"] [bnd;ior;xor] =
  ["and";"ior";"xor"]
    #: [] bool bool bool

(* Integer operators *)
let [@warning "-8"] [add;sub;mul;div;rem] =
  ["add";"sub";"mul";"div";"mod"]
    #: [] int int int




(* Unary operators *)
let not = ! "not" [] [bool] bool
let neg = ! "neg" [] [int] int


(* Array iterators (SOACs) *)
let map  = !"map"  [Size n; Type t;                 Type w] [[t    ] => w; ~:n; n^.t            ] (n^.w)
let map2 = !"map2" [Size n; Type t; Type u;         Type w] [[t;u  ] => w; ~:n; n^.t; n^.u      ] (n^.w)
let map3 = !"map3" [Size n; Type t; Type u; Type v; Type w] [[t;u;v] => w; ~:n; n^.t; n^.u; n^.v] (n^.w)

let fold  = !"fold"  [Size n; Type t;                 Type w] [[w;t    ] => w; ~:n; w; n^.t            ] w
let fold2 = !"fold2" [Size n; Type t; Type u;         Type w] [[w;t;u  ] => w; ~:n; w; n^.t; n^.u      ] w
let fold3 = !"fold3" [Size n; Type t; Type u; Type v; Type w] [[w;t;u;v] => w; ~:n; w; n^.t; n^.u; n^.v] w

let scan  = !"scan"  [Size n; Type t;                 Type w] [[w;t    ] => w; ~:n; w; n^.t            ] (n^.w)
let scan2 = !"scan2" [Size n; Type t; Type u;         Type w] [[w;t;u  ] => w; ~:n; w; n^.t; n^.u      ] (n^.w)
let scan3 = !"scan3" [Size n; Type t; Type u; Type v; Type w] [[w;t;u;v] => w; ~:n; w; n^.t; n^.u; n^.v] (n^.w)


let i_map = [ map; map2; map3 ]
let i_fold = [ fold; fold2; fold3 ]
let i_scan = [ scan; scan2; scan3 ]


(* Array operators (FOACs) *)
let _ = !"transpose" [Size n; Size k; Type t] [n^.k^.t] (k^.n^.t)
let _ = !"window" [Size n; Size k; Type t] [~:k; Size.(n+k-scal 1)^.t] (n^.k^.t)
let _ = !"sample" [Size n; Size k; Type t] [~:k; Size.(n*k-k+scal 1)^.t] (n^.t)
let _ = !"split" [Size n; Size k; Type t]  [~:k; Size.(n*k)^.t] (n^.k^.t)
let _ = !"flatten" [Size n; Size k; Type t]  [~:k; n^.k^.t] (Size.(n*k)^.t)


let builtins = builtins.contents
