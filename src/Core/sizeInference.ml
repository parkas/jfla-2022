(** -------- Size inference -------- **)

open Inference



(**---- Size unification ----------------**)

(* Try to find a simple substitution over variables [FVARS]
   that cancels polynom [P] *)
let cancel fvars p =
  let a_vars = Size.vars p in
  let vars = SSet.inter a_vars fvars in

  let free i p = not (SSet.mem i (Size.vars p)) in

  (* Form P = Q±i | i not in Q *)
  let cancel i = function
    | None when free i Size.(p + unit i) -> Some Size.(i, unit i + p)
    | None when free i Size.(p - unit i) -> Some Size.(i, unit i - p)
    | o -> o
  in

  (* Form P = (i-j)*Q *)
  let try_sub i j = function
    | None when i != j && Size.(compose i (unit j) p =@<compare>@ zero) -> Some (i, Size.unit j)
    | o -> o
  in

  if p =@<Size.compare>@ Size.zero then None
  else None
       |> SSet.fold cancel vars
       |> SSet.fold (fun i -> SSet.fold (try_sub i) a_vars) vars


(* Build a substitution over a subset of [VARS] to
   resolve some of constraints [SIZES]
   NOTE: Resulting sustitutions are ordered: top-level ones may
         contain variables that are substituted by latter ones. *)
let rec find_subst vars sizes =
  match List.find_map (cancel vars) sizes with
  | None -> []
  | Some (i,p) -> sizes
                  |> List.map (Size.compose i p)
                  |> List.filter Size.vars#.SSet.is_empty#.not
                  |> find_subst vars
                  |> List.cons (i,p)



(**---- Type unification ----------------**)

(* Extract size equalities required to ensure type equality.
   WARNING: types are required to be equally structured. t1 <: t2 *)
let unify_types loc t1 t2 =
  let rec uni t1 t2 = match t1,t2 with

    (* Sub-typing *)
    | T_index _, T_base T_int
      | T_size _, T_base T_int -> id

    (* Refinement unification *)
    | T_index s1, T_index s2
      | T_size s1, T_size s2 -> List.cons (s1,s2)

    (* Arrow: mind the variance ! *)
    | T_arrow (d1,c1), T_arrow (d2,c2) -> uni d2 d1 <|> uni c1 c2

    (* Pair: covariant members ! *)
    | T_pair (f1,s1), T_pair (f2,s2) -> uni f1 f2 <|> uni s1 s2

    | T_var _, T_var _ -> id

    (* Sanity check: well typed ! *)
    | t1, t2 -> if t1 = t2 then id else
                  (let v = p_vars () in
                   p_error loc "ICI";
                   Format.printf "Oups unify: <%a> and <%a>"
                     (Printer.tau v 0) t1
                     (Printer.tau v 0) t2;
                   failwith "Incompatible types: type error")
  in uni t1 t2 []









(**---- Unification and substitution ----**)

(* Variable / Size / Type substitution *)
let (~@) subst var = s_subst subst var
let (@.) subst eta = MapS.eta subst eta
let (@:) subst tau = MapS.tau subst tau
let (@>) subst pat = MapS.pat subst pat

type 'a dsc = 'a * Type.t pair list loc

(* Size unifier to collect expression constraints
   u_subst: constains substitutions of variables, with pol with no subst vars... TO EXPLAIN
 *)
type unifier =
  { u_vused : SSet.t
  ; u_fvars : tau loc SMap.t
  ; u_subst : Size.t SMap.t
  ; u_cstrs : Size.t pair dsc list
  ; u_coerc : Size.t pair dsc list
  ; u_s_app : Size.t list EMap.t
  ; u_s_abs : Size.v list EMap.t
  ; u_decls : pat EMap.t }


(* Empty unifier *)
let empty =
  { u_vused = SSet.empty
  ; u_fvars = SMap.empty
  ; u_subst = SMap.empty
  ; u_cstrs = List.empty
  ; u_coerc = List.empty
  ; u_s_app = EMap.empty
  ; u_s_abs = EMap.empty
  ; u_decls = EMap.empty }


(* Functional fields *)
let u_vused () = F.rw (fun u -> u.u_vused) (fun s u -> { u with u_vused = s }) SVar.set
let u_fvars () = F.rw (fun u -> u.u_fvars) (fun m u -> { u with u_fvars = m }) (SVar.map' nop)
let u_subst () = F.rw (fun u -> u.u_subst) (fun m u -> { u with u_subst = m }) (SVar.map' nop)
let u_cstrs () = F.rw (fun u -> u.u_cstrs) (fun l u -> { u with u_cstrs = l }) (list nop)
let u_coerc () = F.rw (fun u -> u.u_coerc) (fun l u -> { u with u_coerc = l }) (list nop)
let u_s_app () = F.rw (fun u -> u.u_s_app) (fun m u -> { u with u_s_app = m }) (EVar.map' ~default:[] (list nop))
let u_decls () = F.rw (fun u -> u.u_decls) (fun m u -> { u with u_decls = m }) (EVar.map' nop)
let u_s_abs () = F.rw (fun u -> u.u_s_abs) (fun m u -> { u with u_s_abs = m }) (EVar.map' ~default:[] (list nop))



(* Add new size variable *)
let add_nfree typ_l svar unif =
  (**) debug 3 "[++] Add fvars: %s\n" (s svar);
  assert (SMap.mem svar unif.u_fvars |> not); (* Variable should not have been registered  *)
  assert (SMap.mem svar unif.u_subst |> not); (* Variable should not have been substituted *)

  unif
  |> u_fvars #=+ svar typ_l


(* Define free size variable [svar] with size [eta].
   The variable is substituted in already defined substitution *)
let add_subst svar eta unif =
  (**) debug 3 "[**] Add Subst: %s := %a\n" (s svar) (Printer.eta pv) eta;

  let eta = unif.u_subst @. eta in
  assert (SSet.mem svar (Vars.eta_sizes eta) |> not); (* Substitution may not be cyclic *)
  assert (SMap.mem svar unif.u_fvars);                (* Substituted variable must be free *)

  let subst = Size.compose svar eta in

  unif
  |> u_subst #<- (SMap.map subst)
  |> u_coerc #<- (List.map (map_fst (Pair.map subst)))
  |> u_cstrs #<- (List.map (map_fst (Pair.map subst)))
  |> u_subst #=+ svar eta
  |> u_fvars #=- svar


(* Add size constraint [s1] = [s2] with location description [dsc] *)
let add_cstr dsc s1 s2 unif =
  (**) debug 3 "[--] Add cstr: %a <: %a@." (Printer.eta pv) s1 (Printer.eta pv) s2;

  let c =
    unif.u_subst @. s1,
    unif.u_subst @. s2
  in

  unif
  |> u_cstrs #=+ (c,dsc)


(* Add size coercion [s1] : [s2] with location description [dsc] *)
let add_coer dsc s1 s2 unif =
  (**) debug 3 "[--] Add coer: (%a : %a)@." (Printer.eta pv) s1 (Printer.eta pv) s2;

  let c =
    unif.u_subst @. s1,
    unif.u_subst @. s2
  in

  unif
  |> u_coerc #=+ (c,dsc)



(* Unifiers merging *)
let (++) u1 u2 =
  { u_vused = SSet.union  u1.u_vused u2.u_vused
  ; u_fvars = SMap.union' u1.u_fvars u2.u_fvars
  ; u_subst = SMap.union' u1.u_subst u2.u_subst
  ; u_coerc = List.append u1.u_coerc u2.u_coerc
  ; u_cstrs = List.append u1.u_cstrs u2.u_cstrs
  ; u_s_app = EMap.union' u1.u_s_app u2.u_s_app
  ; u_s_abs = EMap.union' u1.u_s_abs u2.u_s_abs
  ; u_decls = EMap.union' u1.u_decls u2.u_decls }








(**---- Unifier simplifiction ----------------**)

let diff (s1,s2) = Size.(s1 - s2)

(*  *)
let simplify unif =

  let unif =
    List.fold (uncurry add_subst)
      (find_subst (SMap.keys unif.u_fvars)
         (List.map fst#.diff#.((@.) unif.u_subst) (unif.u_cstrs@unif.u_coerc))) unif
  in

  let cstr (cstr,dsc) =
    let cstr = Pair.map ((@.) unif.u_subst) cstr in
    match Size.to_scalar (diff cstr) with
    | None -> Some (cstr,dsc)
    | Some n -> if n = 0 then None
                else let vars = p_vars () in
                     r_error (snd dsc) "Size %a is not compatible with size %a.\n  @[<v>%a@]"
                       (Printer.eta vars) (fst cstr)
                       (Printer.eta vars) (snd cstr)
                       (List.pp "@;" (fun p (t1,t2) ->
                            Format.fprintf p "while unifying %a and %a"
                              (Printer.tau vars 0) (unif.u_subst @: t1)
                              (Printer.tau vars 0) (unif.u_subst @: t2))) (fst dsc)
  in

  unif
  |> u_cstrs #<- (List.filter_map cstr)
  |> u_coerc #<- (List.filter_map cstr)


(**---- Type unification ----------------**)

(* Add type equality of [T1] and [T2] to unifier [UNIF], by adding
   as many substitution as possible. *)
let unify loc t1 t2 =
  List.fold (add_cstr ([t1,t2],loc) |> uncurry) (unify_types loc t1 t2)

(* Add type equality of [T1] and [T2] to unifier [UNIF], by adding
   as many substitution as possible. *)
let coerce loc t1 t2 =
  let coerce unif =
    List.fold (add_coer ([t1,t2],loc) |> uncurry) (unify_types loc t1 t2) unif
  in
  match t1, t2 with
  | T_size _, T_size _ -> coerce           (* Size equality: coercion constraints *)
  | _, T_size _ | _, T_index _ -> id       (* Range checking: no size constraint introduced *)
  | _ -> coerce







(**---- Inference context ---------------**)

type env =
  { e_vars : pat IMap.t
  ; e_svar : SSet.t }

let e_vars () = F.rw (fun e -> e.e_vars) (fun m e -> { e with e_vars = m }) (imap nop)
let e_svar () = F.rw (fun e -> e.e_svar) (fun s e -> { e with e_svar = s }) SVar.set

let add_var x env = env |> e_vars #=+ (fst x)
let get_var x env = e_vars #.. (fst x) #! env
let add_tvar  _  env = env
let add_svar var env = env |> e_svar #=+ var
let add_decl (x,_,_) pat = add_var x pat


let new_eta () = Size.unit ~!SVar.create





(**---- Polynomial extraction --------------**)

(* Convert static expr to polynom *)
exception Non_poly_exp

let rec get_poly env = function

  | E_size s, _ -> s

  | E_var x, _ ->
     begin match get_var x env with
     | T_mono (T_size s) -> s
     | _ -> raise Non_poly_exp
     end

  | E_cst (C_int i), _ -> Size.scal i

  | E_app ((E_app ((E_var (op,_),_), a),_), b), _ -> begin
      try List.assoc op [ fst#.snd Builtins.add, Size.( + )
                        ; fst#.snd Builtins.sub, Size.( * )
                        ; fst#.snd Builtins.mul, Size.( - ) ]
            (get_poly env a) (get_poly env b)
      with Not_found -> raise Non_poly_exp
    end

  | _ -> raise Non_poly_exp









(*
(* Create new size variables in type *)
let rec renew = function
  | T_size _  -> T_size  (Size.unit ~!SVar.create)
  | T_index _ -> T_index (Size.unit ~!SVar.create)
  | T_base _ | T_var _ | T_pair _ | T_arrow _ as t
    -> AstMap.tau renew t
*)

(* Register fresh variables of tau in unifier *)
let register env loc tau =
  Vars.tau_sizes tau
  |> SSet.fold (fun v ->
         if SSet.mem v env.e_svar
         then u_vused #=+ v
         else add_nfree (tau,loc) v)



(* TODO: to share ! *)

let quant_list pat exp =
  let rec collect l = function
    | T_poly (Type v,p), (E_Abs (Type _, f), _ as e) -> collect ((Type v,e)::l) (p,f)
    | T_poly (Type _,_) as p, (E_Abs (Size _, f), _) -> collect l (p,f)
    | T_poly (Size v,p), e -> collect ((Size v, e)::l) (p,e)
    | T_poly (Type _,_), _ -> failwith "Type abstraction expected"
    | T_mono _, _ -> l
  in collect [] (pat,exp)



(* Size abstraction requires contraints solving.
   Remaining constraints introduced by coercions are removed *)
let abstract_svar var exp unif =
  if SSet.mem var unif.u_vused |> not
  then unif
       |> add_nfree (T_size (Size.unit var), dummy_loc) var
       |> add_subst var (Size.scal 0)
  else
    let unif = simplify unif in
    let used = fst#.diff#.Vars.eta_sizes#.(SSet.mem var) in
    match List.find_opt used unif.u_cstrs with
    | Some (_,(_,l)) -> r_error l "Size variable would escape its scope"
    | None -> unif
              |> add_nfree (T_size (Size.unit var), dummy_loc) var
              |> u_coerc #<- (List.filter used#.not)

(* Type abstraction ignored *)
let abstract_tvar _ unif = unif






let rec meet loc ?(sub=true) t1 t2 =
  match t1,t2 with
  | t1,t2 when t1 = t2 -> t1
  | T_size _, T_size _ -> T_size ~!new_eta
  | T_index _, T_index _ -> T_index ~!new_eta
  | T_size _, _ | _, T_size _ when sub -> T_base T_int
  | T_index _, _ | _, T_index _ when sub -> T_base T_int
  | T_pair (f1,s1), T_pair (f2,s2) -> T_pair (meet loc ~sub f1 f2,
                                              meet loc ~sub s1 s2)
  | T_arrow (d1,c1), T_arrow (d2,c2) ->
     T_arrow (meet loc ~sub:(not sub) d1 d2, meet loc ~sub c1 c2)

  | t1,t2 (*! DFLT tau !*) ->
     let v = p_vars () in
     p_error loc "ICI";
     Format.printf "Oups unify: <%a> and <%a>"
       (Printer.tau v 0) t1
       (Printer.tau v 0) t2;
     failwith "Incompatible types: type error"


(* Constraints and type extraction *)
let rec exp_tau env (exp,(eid,loc)) =
  exp_tau' env eid loc exp

and exp_tau' env eid loc = function

  | E_app (f,e) ->
     let te,ue = exp_tau env e in
     begin match exp_tau env f with
     | T_arrow (t1,t2), u -> t2, ue ++ u |> unify loc te t1
     | _ -> failwith "Function type expected: typing error"
     end

  | E_abs ((x,t),e) ->
     let te,ue = exp_tau (add_var x (T_mono t) env) e in
     t=>te, ue |> register env loc t

  | E_cst c -> c_tau c, empty

  | E_pair (f,s) ->
     let tf,uf = exp_tau env f in
     let ts,us = exp_tau env s in
     T_pair (tf,ts), uf ++ us

  | E_exist ((v,s),e) ->
     let t', unif = exp_tau env s in
     let t, unif' = exp_tau (e_svar #=+ v env) e in
     t, begin match t' with

        (* Formal size may escape *)
        | T_size _ -> let s = Size.unit v in
                      unif ++ unif'
                      |> register env loc (T_size s)
                      |> unify loc (T_size s) t'

        (* Existentially quantified size may not escape *)
        | _ -> (*let svars = Vars.tau_sizes t in*)
               let unif = unif ++ unif' in
(*               let ext,int = SMap.partition (fun i _ -> SSet.mem i svars) unif.u_fvars in*)
               let unif =
                 unif
(*                 |> u_fvars #= int*)
                 |> simplify
(*                 |> u_fvars #<- (SMap.union' ext)*)
               in
               if MapS.tau unif.u_subst t |> Vars.tau_sizes |> SSet.mem v
                  || List.exists fst#.diff#.Size.vars#.(SSet.mem v) unif.u_cstrs
               then r_error loc "Introduced size would escape its scope"
               else assert (SMap.is_empty unif.u_fvars);
               { unif with u_coerc = List.filter fst#.diff#.Size.vars#.(SSet.mem v)#.not unif.u_coerc }
        end

  | E_size n -> T_size n, empty |> register env loc (T_size n)

  | E_let ((x,p,f),e) ->
     let p, unif = decl false env (x,p,f) |> generalize env f in
     let t, u = exp_tau (add_var x p env) e in
     t, u ++ unif

  | E_coer (e,t) ->
     let te,ue = exp_tau env e in
     t, ue
        |> register env loc t
        |> coerce (exp_loc e) te t

  | E_cond (c,(t,f)) ->
     let tc, uc = exp_tau env c in
     let tt, ut = exp_tau env t in
     let tf, uf = exp_tau env f in
     let nt = meet loc tt tf in
     nt, uc ++ ut ++ uf
         |> register env loc nt
         |> unify (exp_loc c) tc (T_base T_bool)
         |> unify (exp_loc t) tt nt
         |> unify (exp_loc f) tf nt

  | E_Abs (Size v, e) -> exp_tau (add_svar v env) e |> map_snd (abstract_svar v e)
  | E_Abs (Type v, e) -> exp_tau (add_tvar v env) e |> map_snd (abstract_tvar v)

  (* Pattern possible *)
  | E_var _ | E_App _ | E_fix _ as exp ->
     match exp_pat env (exp,(eid,loc))
           |> add_inst env eid loc with
     | T_mono tau, unif -> tau, unif
     | _ -> failwith "Monomorphic type expected: typing error"

and exp_pat env (exp,(eid,loc)) =
  exp_pat' env eid loc exp

and exp_pat' env eid loc = function

  | E_var x -> get_var x env, empty
  | E_fix d -> decl true env d

  | E_App (Size _, _) -> r_error loc "Size application not allowed"
  | E_App (Type t, e) ->
     begin match exp_pat env e
                 |> add_inst env (exp_eid e) loc with
     | T_poly (Type v,t'), unif -> MapT.pat (TMap.singleton v t) t',
                                   register env loc t unif
     | _ -> failwith "Type abstraction expected: typing error"
     end

  | e (*! DFLT exp !*) -> failwith "Unexpected pattern expression"


(* Instanciate size patterns and register introduced variables *)
and add_inst env eid loc = function

  (* Insert type instanciation *)
  | T_poly (Size v,p), unif ->
     let n = ~!new_eta in
     (MapS.Fn.pat (Size.compose v n) p, unif)
     |> add_inst env eid loc
     |> map_snd (register env loc (T_size n))
     |> map_snd (u_s_app#..eid #=+ n)

  (* Do not alter remaining patterns  *)
  | p, u (*! DFLT pat !*) -> p, u



(* [SVARS]: extra size variables for the declaration *)
and decl rec_f env (var,pat,exp) =

  let loc = exp_loc exp in

  (* Get pattern description *)
  let tau, (_,q_vars) = pat_desc pat in
  let s_vars = Vars.tau_sizes tau in
  let q_vars = SSet.inter s_vars q_vars in


  (* Collect size contraints and build type pattern *)
  let te,ue = exp_tau (if rec_f then add_var var pat env else env) exp in

  let unif =
    ue
    |> unify loc te tau
    |> register (SSet.fold add_svar q_vars env) loc tau
    |> simplify
  in


  SSet.diff s_vars q_vars
  |> SSet.iter (fun v ->
         Option.iter (fun s ->
             if SSet.inter q_vars (Vars.eta_sizes s) |> SSet.is_empty |> not
             then let vars = p_vars () in
                  r_error loc "This expression has type %a@.  which is less general than type scheme %a"
                    (Printer.tau vars 0) (unif.u_subst@:te)
                    (Printer.pat vars) pat;)
           (SMap.find_opt v unif.u_subst));

  unif.u_cstrs
  |> List.find_opt fst#.(fun cstr ->
         SSet.inter (Vars.eta_sizes (diff cstr)) q_vars
         |> SSet.is_empty |> not)
  |> Option.iter (fun ((s,s'),(_,l)) ->
         let vars = p_vars () in
         r_error l "Cannot unify sizes <%a> and <%a>"
           (Printer.eta vars) s (Printer.eta vars) s'
       );

  (* Build type quantification function *)
  let quanti,unif =
    List.fold_left (fun (quanti,unif) ->
        function
        | Type v, exp -> quanti <|> (poly (Type v)), unif
        | Size v, exp ->
(*           Format.printf "Quantif %s@." (snd pv v);*)
           if SSet.mem v q_vars |> not then quanti,unif else
             let n = ~!SVar.create in
             let p = ~!SVar.create in
             quanti <|> MapS.Fn.pat (Size.compose v (Size.unit p)) <|> poly (Size p),
             unif
             |> u_s_abs #.. (exp_eid exp) #=+ n
             |> add_nfree (T_size (Size.unit v), loc) v
             |> add_subst v (Size.unit n)
      ) ((@:) unif.u_subst <|> mono, unif) (quant_list pat exp)
  in

  let pat = quanti tau in


  let rem_vars = SSet.diff (SMap.keys unif.u_fvars) (Vars.pat_sizes pat) in
  if not (SSet.is_empty rem_vars)
  then begin
      let i = SSet.choose rem_vars in
      let t,l = SMap.find i unif.u_fvars in
      let vars = p_vars () in
      r_error l "Unconstrained variable %s of type <%a>" (snd vars i) (Printer.tau vars 0) t
    end;


  pat,
  unif
  |> u_decls #=+ (exp_eid exp) (quanti tau)








and generalize env exp (pat,unif) =

  let pat = unif.u_subst @> pat in
  let unif = u_decls #.. (exp_eid exp) #<- ((@>) unif.u_subst) unif in

  let gen_vars =
    env.e_svar
    |> IMap.fold (fun _ -> Vars.pat_sizes <|> SSet.union) env.e_vars
    |> SSet.diff (Vars.pat_sizes pat)
  in

  let e_vars = List.fold (fun ((t1,t2),_) ->
                   (unif.u_subst@.t1 |> Vars.eta_sizes |> SSet.union)
                   <|> (unif.u_subst@.t2 |> Vars.eta_sizes |> SSet.union))
                 unif.u_cstrs SSet.empty in
  let gen_vars = SSet.diff gen_vars e_vars in

(*
  Format.printf "// Gen %a (%a) over" (Printer.pat pv) u_decls #.. (exp_eid exp) #! unif (Printer.pat pv) pat;
  SSet.iter (snd pv)#.(Format.printf " %s") gen_vars;
  Format.printf "@.";
*)

  SSet.fold (fun v (pat,unif) ->
      let n = ~!SVar.create in
      let p = ~!SVar.create in
      T_poly (Size v,pat),
      unif
      |> u_s_abs #.. (exp_eid exp) #=+ n
      |> u_decls #.. (exp_eid exp) #<- ((MapS.Fn.pat (Size.compose v (Size.unit p)))#.(poly (Size p)))
      |> add_subst v (Size.unit n))
    gen_vars (pat,unif)







let rec add_expl unif (exp,(eid,loc)) =
  let decl (var,pat,exp) = var,u_decls#..(exp_eid exp)#!unif, exp in
  exp
  |> ( function
       | E_let (d,e) -> E_let (decl d, e)
       | E_fix  d    -> E_fix (decl d)
       | e (*! DFLT exp !*) -> e )
  |> List.fold       (fun s e -> E_App (Size s, (e,(~!EVar.create,loc)))) u_s_app#..eid#!unif
  |> List.fold_right (fun s e -> E_Abs (Size s, (e,(~!EVar.create,loc)))) u_s_abs#..eid#!unif
  |> AstMap.exp (add_expl unif),
  (eid,loc)


(* Avoid annoying starting case *)
let add_expl unif decl =
  match (E_fix decl,(EVar.undef,dummy_loc))
        |> add_expl unif
        |> MapS.exp unif.u_subst
  with
  | E_fix  d, _ -> d
  | _ -> assert false




let check_solved unif =
  unif.u_cstrs
  |> List.find_opt (fun _ -> true)
  |> Option.iter (fun ((s,s'),(_,l)) ->
         let vars = p_vars () in
         r_error l "Cannot unify sizes <%a> and <%a>"
           (Printer.eta vars) s (Printer.eta vars) s'
       );
  unif



let top_decl env ((x,p,e),(t,s)) =
  let env = { e_vars = env;
              e_svar = s }
  in

  let _,unif =
    decl false env (x,p,e)
    |> map_snd (SSet.fold (add_nfree (T_base T_int,exp_loc e)) s <|> simplify <|> check_solved)
    |> generalize (e_svar #= SSet.empty env) e
  in



  if not (SMap.is_empty unif.u_fvars)
  then begin
      let i = SSet.choose (SMap.keys unif.u_fvars) in
      let t,l = SMap.find i unif.u_fvars in
      let vars = p_vars () in
      r_error l "Unconstrained variable %s of type <%a>" (snd vars i) (Printer.tau vars 0) t
    end;


(*
  let unif =
    SMap.fold (fun v _ -> add_subst v (Size.scal 0))
      unif.u_fvars unif
  in
*)

  add_expl unif (x,p,e), (t,SSet.empty)
