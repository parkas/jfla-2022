open Std
open Ast
open Format



type assoc = NA | LtoR | RtoL

let prec_level = function
  | E_var _       -> 16, NA
  | E_cst _       -> 16, NA
  | E_size _      -> 16, NA
  | E_coer _      -> 0, NA
  | E_app _       -> 15, LtoR
  | E_App _       -> 15, LtoR
  | E_pair _      -> 5, NA
  | E_cond _      -> 4, LtoR
  | E_fix _       -> 3,RtoL
  | E_let _       -> 3,RtoL
  | E_exist _     -> 3,RtoL
  | E_abs _       -> 3,RtoL
  | E_Abs _       -> 3,RtoL
let atom = 6


let prec_level_t = function
  | T_var _ -> 16, NA
  | T_base _ -> 16, NA
  | T_size _ -> 16, NA
  | T_index _ -> 16, NA
  | T_pair _ -> 15, NA
  | T_arrow (T_index _, t) -> 15,RtoL
  | T_arrow _ -> 14, RtoL

let base_type_s = function
  | T_int -> "int"
  | T_bool -> "bool"


let var p (v,_) = fprintf p "%s" (Names.to_string v)

let eta vars p = Size.print (snd vars) string_of_int p

let rec tau vars prec p t =
  let level,assoc = prec_level_t t in
  let lP,rP = match assoc with
    | LtoR -> level  , 1+level
    | RtoL -> level+1,   level
    | NA   -> level+1, 1+level
  in
  let f p = function
    | T_var v -> fprintf p "%s" (fst vars v)
    | T_base t -> fprintf p "%s" (base_type_s t)
    | T_size s -> fprintf p "<%a>" (eta vars) s
    | T_index s -> fprintf p "[%a]" (eta vars) s
    | T_pair (t,u) -> fprintf p "%a * %a" (tau vars lP) t (tau vars rP) u
    | T_arrow (T_index s,t) -> fprintf p "[%a]%a" (eta vars) s (tau vars rP) t
    | T_arrow (t,u) -> fprintf p "%a -> %a" (tau vars lP) t (tau vars rP) u
  in
  fprintf p (if level < prec  then "(@[<hov>%a@])" else "@[<hov>%a@]") f t


let pat vars =
  let e = "" in
  let s = "size" in
  let t = "type" in

  let rec pat c p = function
    | T_mono t -> fprintf p "%s%a" (if c = e then "" else ". ") (tau vars 0) t
    | T_poly (v,sg) ->
       let n,v = match v with
         | Size v -> s,snd vars v
         | Type v -> t,fst vars v
       in
       fprintf p "%s %s%a" (if c = n then "" else if c = e then n else ". "^n) v (pat n) sg
  in
  pat e

let cst p = function
  | C_int n -> fprintf p "%d" n
  | C_bool b -> fprintf p (if b then "true" else "false")


let rec exp vars prec p e =
  let level,assoc = prec_level (fst e) in
  let lP,rP = match assoc with
    | LtoR -> level  , 1+level
    | RtoL -> level+1,   level
    | NA   -> level+1, 1+level
  in
  let f p = function
    | E_var (x) -> var p x
    | E_cst c -> cst p c
    | E_size s -> fprintf p "<%a>" (eta vars) s
    (*    | E_pair (a,b) -> fprintf p "(@[<hov>%a,@;%a@])" (exp vars) a (exp vars) b*)
    | E_pair (f,s) -> fprintf p "%a,@;%a" (exp vars lP) f (exp vars rP) s
    | E_coer (e,t) -> fprintf p "%a:@;%a" (exp vars lP) e (tau vars 0) t
    | E_app (f,e) -> fprintf p "%a@;%a" (exp vars lP) f (exp vars rP) e
    | E_abs ((x,t),e) -> fprintf p "fun %a:%a ->@;%a" (var) x (tau vars 15) t (exp vars rP) e
    | E_App (Size s, e) -> fprintf p "%a@;[size %a]" (exp vars lP) e (eta vars) s
    | E_App (Type t, e) -> fprintf p "%a@;[type %a]" (exp vars lP) e (tau vars 0) t
    | E_Abs (Size s, e) -> fprintf p "size %s ->@;%a" (snd vars s) (exp vars rP) e
    | E_Abs (Type t, e) -> fprintf p "type %s ->@;%a" (fst vars t) (exp vars rP) e
    | E_fix d -> fprintf p "@[<hov>%a@]" (decl "fix" vars) d
    | E_let (d,f) -> fprintf p "@[<v>@[<hov>%a@;in@]@;@]%a" (decl "let" vars) d (exp vars rP) f
    | E_exist ((v,e),f) -> fprintf p "@[<v>@[<hov 2>let size %s =@;%a@;in@]@;@]%a" (snd vars v)
                             (exp vars 1) e (exp vars rP) f
    | E_cond (c,(t,f)) -> fprintf p "@[<hov>if %a@;then %a@;else %a"
                            (exp vars 0) c (exp vars 0) t (exp vars rP) f
  in
  fprintf p (if level < prec  then "(@[<hov>%a@])" else "%a") f (fst e)


and decl intro vars p (x,t,e) =
  fprintf p "@[<hov 2>%s %a:%a =@;%a@]" intro var x (pat vars) t
    (exp vars 1) e



let top_decl p (d,_) =
  let _,t = TVar.ids () in
  let _,s = SVar.ids () in
  decl "let" (t,s) p d

let implementation p =
  fprintf p "@[<v>%a@]" (List.pp "@;" top_decl)
