(** -------- Normal Form Checking -------- **)

open Std
open Ast

let rec exp (e,_) = match e with
  | E_var _ | E_cst _ | E_size _ -> ()
  | E_coer (e,_) -> exp e
  | E_pair (f,s) -> exp f; exp s
  | E_app (f,e) -> exp f; exp e
  | E_abs (_,e) -> exp e
  | E_App (_,e) -> exp e
  | E_Abs (_,e) -> exp e
  | E_let (d,e) -> decl d; exp e
  | E_fix d -> decl d
  | E_exist ((_,s),e) -> exp s; exp e
  | E_cond (c,b) -> exp c; Pair.iter exp b

and coer = function
  | E_coer (e,_), _ -> exp e
  | _,(_,loc) -> r_error loc "Coercion expected"

and decl (x,t,e) = exp e

let check (d,_) = decl d
