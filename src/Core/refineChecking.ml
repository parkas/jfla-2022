(** -------- Type checking -------- **)

open Std
open Ast

let rec join loc t1 t2 =
  match t1,t2 with

  | t1,t2 when t1 = t2 -> t1

  (* Pessimist join over refinements *)
  | T_index _, _ | _, T_index _
    | T_size _, _ | _, T_size _
    | T_base T_int, _ | _, T_base T_int -> T_base T_int

  (* Arrow type *)
  | T_arrow (t1,u1), T_arrow (t2,u2) ->
     T_arrow (join loc t2 t1, join loc u1 u2)

  (* Pair type *)
  | T_pair (t1,u1), T_pair (t2,u2) ->
     T_pair (join loc t1 t2, join loc u1 u2)

  (* Errors *)
  | _ -> let v = p_vars () in
         r_error loc "Incompatible types <%a> and <%a>"
           (Printer.tau v 0) t1 (Printer.tau v 0) t2


(* Remove size abstractions on patterns *)
let rec dump_size = function
  | T_poly (Size _,t) -> dump_size t
  | t -> t


(* Check t2 <: t1, ignoring sizes *)
let rec leq_tau loc t1 t2 =
  match t1,t2 with

  | t1,t2 when t1 = t2 -> ()

  (* Sizes doesn't matter *)
  | T_index _, T_index _
    | T_size _, T_size _ -> ()

  (* Integer refinements *)
  | T_base T_int, T_index _
    | T_base T_int, T_size _ -> ()

  (* Arrow type *)
  | T_arrow (t1,u1), T_arrow (t2,u2) ->
     leq_tau loc t2 t1; leq_tau loc u1 u2

  (* Pair type *)
  | T_pair (t1,u1), T_pair (t2,u2) ->
     leq_tau loc t1 t2; leq_tau loc u1 u2

  (* Errors *)
  | _ -> let v = p_vars () in
         r_error loc "Incompatible types <%a> and <%a>"
           (Printer.tau v 0) t1 (Printer.tau v 0) t2


(* Check p2 <: p1, ignoring sizes *)
let leq_pat loc =
  let rec leq subst p1 p2 =
    match dump_size p1, dump_size p2 with

    (* Apply substitution to unify polymorphic type variables *)
    | T_mono t1, T_mono  t2 ->
       MapT.tau subst t2 |> leq_tau loc t1

    (* Build substitution of polymorphic type variables *)
    | T_poly (Type v1, p1), T_poly (Type v2, p2) ->
       leq (TMap.add v2 (T_var v1) subst) p1 p2

    (* Errors *)
    | _ -> let v = p_vars () in
           r_error loc "Incompatible patterns <%a> and <%a>"
             (Printer.pat v) p1 (Printer.pat v) p2
  in leq TMap.empty


(* Check t2 can be coerced to t1 *)
let coerce loc = function

  (* Any top-level integer refinement may be coerced *)
  | T_size _ | T_index _ as t1 ->
     begin function
       | T_base T_int | T_index _ | T_size _ -> ()
       | t2 -> let v = p_vars () in
               r_error loc "Invalid coercion from <%a> to <%a>"
                 (Printer.tau v 0) t1 (Printer.tau v 0) t2
     end

  (* Any sub-type (regardless sizes) may be coerced *)
  | t1 -> leq_tau loc t1


(* Build expression's type pattern *)
let rec exp_pat env (exp,(_,loc) as e) =
  match exp with
  | E_Abs (Size _, e) -> exp_pat env e
  | E_App (Size _, e) -> exp_pat env e
  | E_Abs (Type v, e) -> T_poly (Type v, exp_pat env e)
  | E_App (Type t, e) ->
     begin match exp_pat env e |> dump_size with
     | T_poly (Type v,u) -> MapT.pat (TMap.singleton v t) u
     | s -> r_error loc "Type polymorphic type expected"
     end
  | E_var x -> IMap.find (fst x) env
  | E_fix (x,t,e) -> decl (IMap.add (fst x) t env) (x,t,e); t

  (* Expliciting remaining cases *)
  | E_cst _ | E_pair _ | E_coer _ | E_app _ | E_abs _
    | E_let _ | E_exist _ | E_size _ | E_cond _->
     T_mono (exp_tau env e)


(* Build expression's type *)
and exp_tau env (exp,(_,loc) as e) =
  match exp with
  | E_cst c -> c_tau c
  | E_pair (f,s) -> T_pair (exp_tau env f, exp_tau env s)
  | E_size s -> T_size s
  | E_coer (e,t) -> exp_tau env e |> coerce loc t; t
  | E_app (f,e) ->
     begin match exp_tau env f with
     | T_arrow (t,t') -> exp_tau env e |> leq_tau loc t; t'
     | _ -> r_error (exp_loc f) "Function expected"
     end
  | E_abs ((x,t),e) -> T_arrow (t, exp_tau (IMap.add (fst x) (T_mono t) env) e)
  | E_let ((x,t,e),f) -> decl env (x,t,e); exp_tau (IMap.add (fst x) t env) f
  | E_exist ((_,s),e) -> exp_tau env s |> leq_tau loc (T_base T_int); exp_tau env e
  | E_cond (c,(t,f)) -> exp_tau env c |> leq_tau loc (T_base T_bool);
                        join loc (exp_tau env t) (exp_tau env f)


  (* Expliciting remaining cases *)
  | E_Abs _ | E_App _ | E_var _ | E_fix _ ->
     match exp_pat env e |> dump_size with
     | T_mono t -> t
     | _ -> r_error loc "Monomorphic type expected"


(* Check expression type *)
and decl env (x,t,e) =
  exp_pat env e |> leq_pat (snd#.snd e) t


(* Top-level expression type check *)
let check env ((_,_,(_,(_,loc))) as d,(t,s)) =
  decl env d;
  if not (TSet.is_empty t) then r_error loc "Unexpected annonymous type variables"

