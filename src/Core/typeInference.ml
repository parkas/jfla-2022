(** -------- Type inference -------- **)
(** This implementation closely follows size inference
    algorithm. **)

open Inference


(* This type represente type int and its refinements
   It is distinct from [int] in order to keep annotations
   intact *)
let unref_int = Type.unit TVar.undef
let undef_eta = Size.unit SVar.undef
let new_tau () = Type.unit ~!TVar.create
let new_eta () = Size.unit ~!SVar.create

(* Converts occurrences of integer refinements to unref_int
   If strict is non-None, checks that unref_int does not appear in type *)
let rec uni_tau = function
  | T_base T_int | T_size _ | T_index _ -> unref_int
  | t when t = unref_int -> failwith "Unexpected non refined type"
  | t (*! DFLT tau !*) -> AstMap.tau uni_tau t

let rec uni_pat = function
  | T_mono t -> T_mono (uni_tau t)
  | T_poly (v,p) -> T_poly (v, uni_pat p)


let rec is_uni_tau = function
  | T_base T_int | T_size _ | T_index _ -> false
  | T_pair (t,u) | T_arrow (t,u) -> is_uni_tau t && is_uni_tau u
  | T_var _ | T_base T_bool -> true

let rec is_uni_pat = function
  | T_poly (_,p) -> is_uni_pat p
  | T_mono t     -> is_uni_tau t

let replace var tau v = if v = var then tau else T_var v







(**---- Unification and substitution ----**)

(* Type / pattern substitution *)
let (@:) subst tau = MapT.tau subst tau
let (@>) subst tau = MapT.pat subst tau

type 'a dsc = 'a * Type.t pair list loc

(* Type unifier to collect expression constraints
   u_subst: constains substitutions of variables, with pol with no subst vars... TO EXPLAIN
   Invariants ?
 *)
type unifier =
  { u_vused : TSet.t
  ; u_fvars : unit loc TMap.t
  ; u_subst : Type.t TMap.t
  ; u_coerc : Type.t pair dsc list
  ; u_cstrs : Type.t pair dsc list
  ; u_s_app : Type.t list EMap.t
  ; u_s_abs : Type.v list EMap.t
  ; u_decls : pat EMap.t }


(* Empty unifier *)
let empty =
  { u_vused = TSet.empty
  ; u_fvars = TMap.empty
  ; u_subst = TMap.empty
  ; u_coerc = List.empty
  ; u_cstrs = List.empty
  ; u_s_app = EMap.empty
  ; u_s_abs = EMap.empty
  ; u_decls = EMap.empty }


(* Functional fields *)
let u_vused () = F.rw (fun u -> u.u_vused) (fun s u -> { u with u_vused = s }) TVar.set
let u_fvars () = F.rw (fun u -> u.u_fvars) (fun m u -> { u with u_fvars = m }) (TVar.map' nop)
let u_subst () = F.rw (fun u -> u.u_subst) (fun m u -> { u with u_subst = m }) (TVar.map' nop)
let u_coerc () = F.rw (fun u -> u.u_coerc) (fun l u -> { u with u_coerc = l }) (List.prop nop)
let u_cstrs () = F.rw (fun u -> u.u_cstrs) (fun l u -> { u with u_cstrs = l }) (List.prop nop)
let u_s_app () = F.rw (fun u -> u.u_s_app) (fun m u -> { u with u_s_app = m }) (EVar.map' ~default:[] (list nop))
let u_s_abs () = F.rw (fun u -> u.u_s_abs) (fun m u -> { u with u_s_abs = m }) (EVar.map' ~default:[] (list nop))
let u_decls () = F.rw (fun u -> u.u_decls) (fun m u -> { u with u_decls = m }) (EVar.map' nop)



(* Add new type variable *)
let add_nfree loc tvar unif =
  (**) debug 3 "[++] Add fvars: %s@." (t tvar);
  assert (tvar <> TVar.undef); (* This one has a special meaning *)
  assert (TMap.mem tvar unif.u_fvars |> not); (* Variable should not have been registered  *)
  assert (TMap.mem tvar unif.u_subst |> not); (* Variable should not have been substituted *)

  unif
  |> u_fvars #=+ tvar ((),loc)


(* Define free type variable [tvar] with type [tau].
   The variable is substituted in already defined substitutions and constraints *)
let add_subst tvar tau unif =
  (**) debug 3 "[**] Add Subst: %s := %a@." (t tvar) (Printer.tau pv 0) tau;

  let tau = unif.u_subst @: tau in
  assert (TSet.mem tvar (Vars.tau_types tau) |> not); (* Substitution may not be cyclic *)
  assert (TMap.mem tvar unif.u_fvars);                (* Substituted variable must be free *)

  let subst = MapT.Fn.tau (replace tvar tau) in

  unif
  |> u_subst #<- (TMap.map subst)
  |> u_coerc #<- (List.map (map_fst (Pair.map subst)))
  |> u_cstrs #<- (List.map (map_fst (Pair.map subst)))
  |> u_subst #=+ tvar tau
  |> u_fvars #=- tvar



(* Add type constraint [t1] <: [t2] at location [loc] *)
let add_cstr loc t1 t2 unif =
  (**) debug 3 "[--] Add cstr: %a <: %a@." (Printer.tau pv 0) t1 (Printer.tau pv 0) t2;
  assert (is_uni_tau t1); (* Type constraint should have *)
  assert (is_uni_tau t2); (*          unified refinements *)

  let c =
    unif.u_subst @: t1,
    unif.u_subst @: t2
  in

  unif
  |> u_cstrs #=+ (c,([c],loc))


(* Add type coercion [t1] : [t2] at location [loc] *)
let add_coer loc t1 t2 unif =
  (**) debug 3 "[--] Add coer: (%a : %a)@." (Printer.tau pv 0) t1 (Printer.tau pv 0) t2;
  assert (is_uni_tau t1); (* Type constraint should have *)
  assert (is_uni_tau t2); (*          unified refinements *)

  let c =
    unif.u_subst @: t1,
    unif.u_subst @: t2
  in

  unif
  |> u_coerc #=+ (c,([c],loc))



(* Unifiers merging *)
let (++) u1 u2 =
  { u_vused = TSet.union  u1.u_vused u2.u_vused
  ; u_fvars = TMap.union' u1.u_fvars u2.u_fvars
  ; u_subst = TMap.union' u1.u_subst u2.u_subst
  ; u_coerc = List.append u1.u_coerc u2.u_coerc
  ; u_cstrs = List.append u1.u_cstrs u2.u_cstrs
  ; u_s_app = EMap.union' u1.u_s_app u2.u_s_app
  ; u_s_abs = EMap.union' u1.u_s_abs u2.u_s_abs
  ; u_decls = EMap.union' u1.u_decls u2.u_decls }





(**---- Unifier simplifiction ----------------**)

(* Distribute subtyping over equally structured part of types *)
let rec distrib = function
  | T_arrow (d1,c1), T_arrow (d2,c2) -> distrib (d2,d1) @ distrib (c1,c2)
  | T_pair (f1,s1) , T_pair (f2,s2)  -> distrib (f1,f2) @ distrib (s1,s2)
  | p (*! DFLT tau !*) -> [p]


(* Turn as many constraints as possible into substitutions of free variables *)
let simplify unif =
  assert (TMap.mem TVar.undef unif.u_fvars |> not); (* Protecting unrefined integer type *)
  debug 3 "// Simplify !@.";

  let add_subst loc v t =
    if Vars.tau_types t |> TSet.mem v |> not
    then add_subst v t
    else let vars = p_vars () in
         r_error loc "Variable %s occurs in type %a"
           (fst vars v) (Printer.tau vars 0) t
  in

  let solve_atom (origin,loc) cstr =
    assert (fst#.is_uni_tau cstr); (* Type constraint should have *)
    assert (snd#.is_uni_tau cstr); (*         unified refinements *)

    match cstr with
    | t1,t2 when t1 = t2 -> id
    | T_var v, t when TMap.mem v unif.u_fvars -> add_subst loc v t
    | t, T_var v when TMap.mem v unif.u_fvars -> add_subst loc v t
    | T_var _, _ | _, T_var _ as cstr -> u_cstrs #=+ (cstr,(origin,loc))
    | t1,t2 (*! DFLT tau !*) ->
       let vars = p_vars () in
       r_error loc "Incompatible types <%a> and <%a>"
         (Printer.tau vars 0) t1 (Printer.tau vars 0) t2
  in

  let solve_cstr (cstr,dsc) =
    let rec solve_cstr cstr unif =
      match distrib (Pair.map ((@:) unif.u_subst) cstr) with
      | [] -> assert false (* At least one constraint ! *)
      | cstr::cstrs -> unif
                       |> solve_atom dsc cstr
                       |> List.fold solve_cstr cstrs
    in solve_cstr cstr
  in


  unif
  |> u_cstrs #= []
  |> u_coerc #= []
  |> List.fold solve_cstr unif.u_cstrs
  |> List.fold solve_cstr unif.u_coerc
(*
  unif
  |> u_subst #<- (TMap.map ((@:) subst))
  |> u_subst #<- (TMap.union' subst)
  |> u_fvars #<- (TSet.fold TMap.remove' (TMap.keys subst))
  |> u_cstrs #= (List.concat cstrs |> List.filter_map id)
  |> u_coerc #= []
*)



(* Debug helper *)
let print_cstr pref unif =
  let vars = p_vars () in
  Format.printf "%s@." pref;
  List.iter (fun ((t,u),_) -> Format.printf "  %a <: %a@."
                                (Printer.tau vars 0) t
                                (Printer.tau vars 0) u) unif.u_cstrs;
  List.iter (fun ((t,u),_) -> Format.printf "  (%a : %a)@."
                                (Printer.tau vars 0) t
                                (Printer.tau vars 0) u) unif.u_coerc;
  unif





(**---- Type unification ----------------**)

(* Add type constraint [t2] <: [t1]. *)
let unify loc t1 t2 =
  add_cstr loc t1 t2

(* Add type equality of [T1] and [T2] to unifier [UNIF], by adding
   as many substitution as possible. *)
let coerce loc t1 t2 =
  add_coer loc t1 t2




(**---- Inference context ---------------**)

(* e_decl contains fully typed declarations
   e_vars contains unrefined declarations *)
type env =
  { e_decl : pat IMap.t
  ; e_vars : pat IMap.t
  ; e_tvar : TSet.t }

let e_vars () = F.rw (fun e -> e.e_vars) (fun m e -> { e with e_vars = m }) (imap nop)
let e_decl () = F.rw (fun e -> e.e_decl) (fun m e -> { e with e_decl = m }) (imap nop)
let e_tvar () = F.rw (fun e -> e.e_tvar) (fun s e -> { e with e_tvar = s }) TVar.set

let add_var x pat = assert (is_uni_pat pat);
                    e_vars #=+ (fst x) pat
let get_loc x env = e_vars #.. (fst x) #! env
let get_glo x env = e_decl #.. (fst x) #! env
let get_var x env =
  try get_loc x env
  with Int.Not_found _ -> uni_pat (get_glo x env)
let add_tvar var env = env |> e_tvar #=+ var
let add_svar  _  env = env
let add_decl (x,_,_) pat =
  assert (is_uni_pat pat); (* Local vars should have unified refinements *)
  add_var x pat


(* Register fresh variables of tau in unifier *)
let register env loc tau =
  Vars.tau_types tau
  |> TSet.remove TVar.undef
  |> TSet.fold (fun v ->
         if TSet.mem v env.e_tvar
         then u_vused #=+ v
         else add_nfree loc v)




let quant_list pat exp =
  let rec collect l = function
    | T_poly (v,p), e -> collect ((v,e)::l) (p,e)
    | T_mono _,_ -> l
  in collect [] (pat,exp)



(* Size abstraction ignored *)
let abstract_svar _ unif = unif

(* Type abstraction requires contraints solving *)
let abstract_tvar var unif =
  assert (var <> TVar.undef);
  if TSet.mem var unif.u_vused |> not
  then unif
       |> add_nfree dummy_loc var
       |> add_subst var unref_int
  else
    let unif = simplify unif in
    match List.find_opt fst#.(fun (t1,t2) ->
      TSet.union (Vars.tau_types t1) (Vars.tau_types t2)
      |> TSet.mem var) (unif.u_coerc @ unif.u_cstrs) with
    | Some (_,(_,l)) -> r_error l "Type variable would escape its scope"
    | None -> add_nfree dummy_loc var unif




(* Constraints and type extraction *)
let rec exp_tau env (exp,(eid,loc)) =
  exp_tau' env eid loc exp

and exp_tau' env eid loc = function

  | E_app (f,e) ->
     let v = ~!new_tau in
     let tf,uf = exp_tau env f in
     let te,ue = exp_tau env e in
     v, uf ++ ue
        |> register env loc v
        |> unify (exp_loc e) tf (te=>v)

  | E_abs ((x,t),e) ->
     let t = uni_tau t in
     let te,ue = exp_tau (add_var x (T_mono t) env) e in
     t=>te, ue
             |> register env loc t

  | E_cst c -> uni_tau (c_tau c), empty

  | E_pair (f,s) ->
     let tf,uf = exp_tau env f in
     let ts,us = exp_tau env s in
     T_pair (tf,ts), uf ++ us

  | E_exist ((v,s),e) ->
     let ts, us = exp_tau env s in
     let te, ue = exp_tau env e in
     te, us ++ ue
         |> unify (exp_loc s) ts (uni_tau (T_base T_int))

  | E_size n ->
     let t =  uni_tau (T_size n) in
     t, empty
        |> register env loc (T_size n)

  | E_let ((x,p,f),e) ->
     let p, up = decl false env (x,p,f) |> generalize env f in
     let t, ut = exp_tau (add_var x p env) e in
     t, ut ++ up

  | E_coer (e,t) ->
     let t = uni_tau t in
     let te,ue = exp_tau env e in
     t, ue
        |> register env loc t
        |> coerce (exp_loc e) te t

  | E_cond (c,(t,f)) ->
     let v = ~!new_tau in
     let tc, uc = exp_tau env c in
     let tt, ut = exp_tau env t in
     let tf, uf = exp_tau env f in
     v, uc ++ ut ++ uf
        |> register env loc v
        |> unify (exp_loc c) tc (T_base T_bool)
        |> unify (exp_loc t) tt v
        |> unify (exp_loc f) tf v

  | E_Abs (Size v, e) -> exp_tau (add_svar v env) e |> map_snd (abstract_svar v)
  | E_Abs (Type v, e) -> exp_tau (add_tvar v env) e |> map_snd (abstract_tvar v)


  (* Pattern possible *)
  | E_var _ | E_App _ | E_fix _ as exp ->
     match exp_pat env (exp,(eid,loc))
           |> add_inst env eid loc with
     | T_mono t, unif -> t, unif
     | _ -> failwith "Unexpected pattern"



(* Extract expression type pattern *)
and exp_pat env (exp,(eid,loc)) =
  exp_pat' env eid loc exp

and exp_pat' env eid loc = function

  | E_var x -> get_var x env, empty
  | E_fix d -> decl true env d

  | E_App (Size _, _) -> r_error loc "Size application not allowed"
  | E_App (Type _, _) -> r_error loc "Type application not allowed"

  | e (*! DFLT exp !*) -> failwith "Unexpected pattern expression"


(* Instanciate type patterns and register introduced variables *)
and add_inst env eid loc = function

  (* Ignore size quantification *)
  | T_poly (Size v,p), unif ->
     add_inst env eid loc (p,unif)

  (* Insert type instanciation *)
  | T_poly (Type v,p), unif ->
     let n = ~!new_tau in
     (MapT.Fn.pat (replace v n) p, unif)
     |> add_inst env eid loc
     |> map_snd (register env loc n)
     |> map_snd (u_s_app#..eid #=+ n)

  (* Do not alter remaining patterns  *)
  | p, u (*! DFLT pat !*) -> p, u


(* [SVARS]: extra size variables for the declaration *)
and decl rec_f env (var,pat,exp) =

  let loc = exp_loc exp in

  (* Get pattern description *)
  let tau, (q_vars, _) = pat_desc pat in
  let t_vars = Vars.tau_types tau in
  let q_vars = TSet.inter t_vars q_vars in


  let u_tau = uni_tau tau in

  (* Collect exp type *)
  let te,ue = exp_tau (if rec_f then add_var var (uni_pat pat) env else env) exp in

  let unif =
    ue
    |> unify loc te u_tau
    |> register (TSet.fold add_tvar q_vars env) loc u_tau
    |> simplify
  in

  TSet.diff t_vars q_vars
  |> TSet.iter (fun v ->
         Option.iter (fun t ->
             if TSet.inter q_vars (Vars.tau_types t) |> TSet.is_empty |> not
             then let vars = p_vars () in
                  r_error loc "This expression has type %a@.  which is less general than type scheme %a"
                    (Printer.tau vars 0) (unif.u_subst@:te)
                    (Printer.pat vars) pat;)
           (TMap.find_opt v unif.u_subst));

  unif.u_cstrs
  |> List.find_opt fst#.(fun (t1,t2) ->
         TSet.inter (Vars.tau_types (T_arrow (t1,t2))) q_vars
         |> TSet.is_empty |> not)
  |> Option.iter (fun ((t,t'),(_,l)) ->
         let vars = p_vars () in
         r_error l "Cannot unify types <%a> and <%a>"
           (Printer.tau vars 0) t (Printer.tau vars 0) t'
       );

  (* Build type quantification function *)
  let quanti,unif =
    List.fold_left (fun (quanti,unif) ->
        function
        | Size v,_ -> quanti <|> (poly (Size v)), unif
        | Type v,exp ->
           if TSet.mem v q_vars |> not then quanti,unif else
             let n = ~!TVar.create in
             let p = ~!TVar.create in
             quanti <|> MapT.Fn.pat (replace v (Type.unit p)) <|> poly (Type p),
             unif
             |> u_s_abs #.. (exp_eid exp) #=+ n
             |> add_nfree loc v
             |> add_subst v (Type.unit n)
      ) ((@:) unif.u_subst <|> mono, unif) (quant_list pat exp)
  in


  quanti u_tau,
  unif
  |> u_decls #=+ (exp_eid exp) (quanti tau)



and generalize env exp (pat,unif) =

  let pat = unif.u_subst @> pat in
  let unif = u_decls #.. (exp_eid exp) #<- ((@>) unif.u_subst) unif in

  let e_vars = List.fold (fun ((t1,t2),_) ->
                   (unif.u_subst@:t1 |> Vars.tau_types |> TSet.union)
                   <|> (unif.u_subst@:t2 |> Vars.tau_types |> TSet.union))
                 unif.u_cstrs TSet.empty in

  let gen_vars =
    TSet.diff (TSet.inter (Vars.pat_types pat) (TMap.keys unif.u_fvars)) e_vars
  in



(*
  Format.printf "// Env vars";
  TSet.iter (fst pv)#.(Format.printf " %s") e_vars;
  Format.printf "@.";

  print_cstr "// cstr:" unif |> ignore;


  Format.printf "// Gen %a (%a) over" (Printer.pat pv) u_decls #.. (exp_eid exp) #! unif (Printer.pat pv) pat;
  TSet.iter (fst pv)#.(Format.printf " %s") gen_vars;
  Format.printf "@.";
*)

  TSet.fold (fun v (pat,unif) ->
      let n = ~!TVar.create in
      let p = ~!TVar.create in
      T_poly (Type v,pat),
      unif
      |> u_s_abs #.. (exp_eid exp) #=+ n
      |> u_decls #.. (exp_eid exp) #<- ((MapT.Fn.pat (replace v (Type.unit p)))#.(poly (Type p)))
      |> add_subst v (Type.unit n))
    gen_vars (pat,unif)





let rec add_expl unif (exp,(eid,loc)) =
  let decl (var,pat,exp) = var,u_decls#..(exp_eid exp)#!unif, exp in
  exp
  |> ( function
       | E_let (d,e) -> E_let (decl d, e)
       | E_fix  d    -> E_fix (decl d)
       | e (*! DFLT exp !*) -> e )
  |> List.fold       (fun s e -> E_App (Type s, (e,(~!EVar.create,loc)))) u_s_app#..eid#!unif
  |> List.fold_right (fun s e -> E_Abs (Type s, (e,(~!EVar.create,loc)))) u_s_abs#..eid#!unif
  |> AstMap.exp (add_expl unif),
  (eid,loc)


(* Avoid annoying starting case *)
let add_expl unif decl =
  match (E_fix decl,(EVar.undef,dummy_loc))
        |> add_expl unif
        |> MapT.exp unif.u_subst
  with
  | E_fix  d, _ -> d
  | _ -> assert false



let check_solved unif =
  unif.u_cstrs
  |> List.find_opt (fun _ -> true)
  |> Option.iter (fun ((t,t'),(_,l)) ->
         let vars = p_vars () in
         r_error l "Cannot unify types <%a> and <%a>"
           (Printer.tau vars 0) t (Printer.tau vars 0) t'
       );
  unif



let top_decl env ((x,p,e),(t,s)) =
  let env = { e_decl = env
            ; e_vars = IMap.empty
            ; e_tvar = t }
  in

  let _,unif =
    decl false env (x,p,e)
    |> map_snd (TSet.fold (add_nfree (exp_loc e)) t <|> simplify <|> check_solved)
    |> generalize (e_tvar #= TSet.empty env) e
  in


  if not (TMap.is_empty unif.u_fvars)
  then begin
      let i = TSet.choose (TMap.keys unif.u_fvars) in
      let _,l = TMap.find i unif.u_fvars in
      let vars = p_vars () in
      r_error l "Unconstrained variable %s" (fst vars i)
    end;

(* (* Default type for unconstraint size variables *)
  let unif =
    TMap.fold (fun v _ -> add_subst v (T_base T_bool))
      unif.u_fvars unif
  in
*)

  add_expl unif (x,p,e), (TSet.empty,s)


