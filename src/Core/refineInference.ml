(** -------- Refinement inference -------- **)
(** This implementation closely follows size inference
    algorithm. **)


open Inference
open TypeInference

type unifier =
  { u_fvars : unit loc TMap.t
  ; u_coerc : Type.t pair loc list
  ; u_cstrs : Type.t pair loc list }


(* Empty unifier *)
let empty =
  { u_fvars = TMap.empty
  ; u_coerc = List.empty
  ; u_cstrs = List.empty }

(* Functional fields *)
let u_fvars () = F.rw (fun u -> u.u_fvars) (fun m u -> { u with u_fvars = m }) (TVar.map' nop)
let u_coerc () = F.rw (fun u -> u.u_coerc) (fun l u -> { u with u_coerc = l }) (list nop)
let u_cstrs () = F.rw (fun u -> u.u_cstrs) (fun l u -> { u with u_cstrs = l }) (list nop)




let add_cstr loc t1 t2 unif =
  debug 3 "[++] Add cstr: %a <: %a@." (Printer.tau pv 0) t1 (Printer.tau pv 0) t2;
  unif
  |> u_cstrs #=+ ((t1,t2),loc)

let add_coer loc t1 t2 unif =
  debug 3 "[++] Add coer: (%a : %a)@." (Printer.tau pv 0) t1 (Printer.tau pv 0) t2;
  unif
  |> u_coerc #=+ ((t1,t2),loc)

(* Unifiers merging *)
let (++) u1 u2 =
  { u_fvars = TMap.union' u1.u_fvars u2.u_fvars
  ; u_coerc = List.append u1.u_coerc u2.u_coerc
  ; u_cstrs = List.append u1.u_cstrs u2.u_cstrs }


let rec unify_pat loc p1 p2 =
  match p1,p2 with
  | T_mono t1, T_mono t2 -> add_cstr loc t1 t2
  | T_poly (Size _, p1), p2 | p1, T_poly (Size _, p2) -> unify_pat loc p1 p2
  | T_poly (Type v, p1), T_poly (Type u, p2) -> unify_pat loc p1 (MapT.Fn.pat (replace u (T_var v)) p2)
  | _ -> assert false

(* e_decl contains fully typed declarations
   e_vars contains unrefined declarations *)
type env =
  { e_vars : pat IMap.t
  ; e_tvar : TSet.t }



let e_vars () = F.rw (fun e -> e.e_vars) (fun m e -> { e with e_vars = m }) (imap nop)
let e_tvar () = F.rw (fun e -> e.e_tvar) (fun s e -> { e with e_tvar = s }) TVar.set'

let add_var x pat = e_vars #=+ (fst x) pat
let get_var x env = e_vars #.. (fst x) #! env


(* Register fresh variables of tau in unifier *)
let register env loc tau =
  TSet.fold (fun v -> u_fvars #=+ v ((),loc)) (TSet.diff (Vars.tau_types tau) env.e_tvar)

let rec register_pat env loc = function
  | T_mono t -> register env loc t
  | T_poly (Size _, p) -> register_pat env loc p
  | T_poly (Type v, p) -> register_pat (e_tvar #=+ v env) loc p



let rec dump_size = function
  | T_poly (Size _, p) -> dump_size p
  | p (*! DFLT pat !*) -> p

let renew env tau =
  MapT.Fn.tau (fun v -> if TSet.mem v env.e_tvar then T_var v else ~!new_tau) (uni_tau tau)

let rec exp_tau env (exp,(eid,loc)) =
  exp_tau' env eid loc exp

and exp_tau' env eid loc = function

  | E_app (f,e) ->
     let tf,uf = exp_tau env f in
     let te,ue = exp_tau env e in
     begin match tf with
     | T_arrow (t,t') ->
        t', uf ++ ue
            |> add_cstr loc te t
     | _ -> failwith "Function expected"
     end

  | E_abs ((x,t),e) ->
     let tau,unif = exp_tau (add_var x (T_mono t) env) e in
     T_arrow (t,tau), unif |> register env loc t

  | E_cst c -> c_tau c, empty

  | E_pair (f,s) ->
     let tf,uf = exp_tau env f in
     let ts,us = exp_tau env s in
     T_pair (tf,ts), uf ++ us

  | E_exist ((v,s),e) ->
     let ts,us = exp_tau env s in
     let te,ue = exp_tau env e in
     te, us ++ ue

  | E_size n -> T_size n, empty |> register env loc (T_size n)

  | E_let ((x,p,f),e) ->
     let _,unif = decl false env TSet.empty (x,p,f) in
     let t, u = exp_tau (add_var x p env) e in
     t, u ++ unif

  | E_coer (e,t) ->
     let t',unif = exp_tau env e in
     t, unif
        |> add_coer loc t' t
        |> register env loc t

  | E_cond (c,(t,f)) ->
     let tc, uc = exp_tau env c in
     let tt, ut = exp_tau env t in
     let tf, uf = exp_tau env f in
     let nt = renew env tt in
     nt, uc ++ ut ++ uf
         |> register env loc nt
         |> add_cstr (exp_loc c) tc (T_base T_bool)
         |> add_cstr (exp_loc t) tt nt
         |> add_cstr (exp_loc f) tf nt


  (* Pattern possible *)
  | E_var _ | E_App _ | E_Abs _ | E_fix _ as exp ->
     begin match exp_pat env (exp,(eid,loc)) |> map_fst dump_size with
     | T_mono tau, unif -> tau, unif
     | _ -> failwith "Monomorphic type expected: typing error"
     end

and exp_pat env (exp,(eid,loc)) =
  exp_pat' env eid loc exp

and exp_pat' env eid loc = function

  | E_var x -> get_var x env, empty
  | E_fix d -> decl true env TSet.empty d

  | E_App (Size _, e) -> exp_pat env e
  | E_Abs (Size _, e) -> exp_pat env e

  | E_App (Type t, e) ->
     begin match exp_pat env e |> map_fst dump_size with
     | T_poly (Type v, t'), unif -> MapT.pat (TMap.singleton v t) t',
                                    register env loc t unif
     | _ -> failwith "Type abstraction expected: typing error"
     end

  | E_Abs (Type v, e) ->
     let pat,unif = exp_pat (e_tvar #=+ v env) e in
     if Vars.pat_types pat |> TSet.mem v then T_poly (Type v, pat), unif
     else failwith "Generalization on unused type var: typing error"

  | E_app _ | E_abs _ | E_cst _ | E_pair _ | E_exist _ | E_size _ | E_let _ | E_coer _ | E_cond _ as exp ->
     let tau,unif = exp_tau env (exp,(eid,loc)) in
     T_mono tau, unif





(* [SVARS]: extra size variables for the declaration *)
and decl rec_f env tvars (var,pat,exp) =


  (* Collect size contraints and build type pattern *)
  let eid,loc = snd exp in
  let p,unif = exp_pat (if rec_f then add_var var pat env else env) exp in
  let unif =
    unif
    |> register_pat env loc pat
    |> unify_pat loc p pat
  in

  pat, unif





(* Decide whether constraint is useful *)
let cstr_filter tvars (cstr,loc) =
  let int_var v = TMap.mem v tvars in
  match cstr with
  | t1,t2 when t1 = t2 -> false
  | T_size _, T_size _ -> false
  | T_index _, T_index _ -> false
  | T_var v1, T_var v2 when int_var v1 && int_var v2 -> true

  | T_base T_int, T_var v | T_var v, T_index _ | T_var v, T_size _ when int_var v -> true
  | T_var v, T_base T_int | T_index _, T_var v | T_size _, T_var v when int_var v -> true

  | T_index _, T_base T_int | T_size _, T_base T_int -> true
  | T_base T_int, T_index _ | T_base T_int, T_size _ -> true

  | t1,t2 (*! DFLT tau !*) ->
     debug 1 "@.Inexpected cstr %a <: %a\n" (Printer.tau ~!p_vars 0) t1 (Printer.tau ~!p_vars 0) t2;
     failwith "Unexpected refinement constraint"


let coer_filter tvars (cstr,loc) = false
(*  let tau_var v = not (TMap.mem v tvars) in
  match cstr with
  | T_var v1, T_var v2 -> tau_var v1 || tau_var v2
  | T_base T_int, T_index _ | T_base T_int, T_size _ -> false
  | T_base T_int, T_var v | T_var v, T_index _ | T_var v, T_size _ -> tau_var v
  | _ -> true*)


let solve unif =
  let tvars = TMap.map (fun _ -> T_base T_int) unif.u_fvars in


  let get_subst = function
    | T_base T_int, T_var v -> Some (v, T_base T_int)
    | T_var v, T_size _     -> Some (v, T_size ~!new_eta)
    | T_var v, T_index _    -> Some (v, T_index ~!new_eta)
    | _ (*! DFLT tau !*) -> None
  in

  (* If one constraint [_] <: 'i or <_> <: 'i only *)
  let sel_subst cstrs =
    TMap.fold (fun v _ -> function
        | Some s -> Some s
        | None -> match List.filter fst#.snd#.((=) (T_var v)) cstrs with
                  | [(T_size _,  T_var v),_] -> Some (v, T_size ~!new_eta)
                  | [(T_index _, T_var v),_] -> Some (v, T_index ~!new_eta)
                  | _ -> None) unif.u_fvars None
  in

  let subst_cstr v t (cstr,loc) =
    let map = function
      | T_var v' when v' = v -> t
      | t (*! DFLT tau !*) -> t
    in
    match Pair.map map cstr with
    | T_base T_int, T_base T_int
      | T_index _, T_index _
      | T_size _, T_size _ -> None

    | T_index _, T_base T_int | T_size _, T_base T_int -> None
    | T_base T_int, T_var _ | T_var _, T_index _ | T_var _, T_size _ as cstr -> Some (cstr,loc)
    | T_var _, T_base T_int | T_index _, T_var _ | T_size _, T_var _ as cstr -> Some (cstr,loc)
    | T_var _, T_var _ as cstr -> Some (cstr,loc)
    | t1,t2 ->
       let vars = p_vars () in
       r_error loc "Incompatible integer refinements <%a> and <%a>"
         (Printer.tau vars 0) t1 (Printer.tau vars 0) t2

  in

  let rec solve subst cstrs =
    let add_subst v t =
      cstrs
      |> List.filter_map (subst_cstr v t)
      |> solve (TMap.add v t subst)
    in
    match List.find_map fst#.get_subst cstrs with
    | Some (v,t) -> add_subst v t
    | None -> match sel_subst cstrs with
              | Some (v,t) -> add_subst v t
              | None ->
(*                 List.iter (fun ((t1,t2),_) ->
                     Format.printf " * Rem cstr %a <: %a@."
                       (Printer.tau pv 0) t1 (Printer.tau pv 0) t2) cstrs;*)
                 subst
  in

(*
  List.iter (fun ((t1,t2),_) ->
      Format.printf " * Coer %a <: %a@."
        (Printer.tau pv 0) t1 (Printer.tau pv 0) t2) unif.u_coerc;
  List.iter (fun ((t1,t2),_) ->
      Format.printf " * Cstr %a <: %a@."
        (Printer.tau pv 0) t1 (Printer.tau pv 0) t2) unif.u_cstrs;
  Format.printf " ***";
  TMap.iter (fun i _ -> Format.printf " %s" (fst pv i)) tvars;
  Format.printf "@.";
*)
  let cstrs =
    unif.u_coerc
    |> List.filter (coer_filter tvars)
    |> List.append unif.u_cstrs
    |> List.map (fun (c,l) -> List.map (fun c -> c,l) (distrib c))
    |> List.concat
    |> List.filter (cstr_filter tvars)
  in
(*
  List.iter (fun ((t1,t2),_) ->
      Format.printf " * Cstr %a <: %a@."
        (Printer.tau pv 0) t1 (Printer.tau pv 0) t2) cstrs;
*)
  cstrs
  |> solve tvars



(*    List.iter (fun ((t1,t2),_) ->
        Format.printf " * Coer %a <: %a@."
          (Printer.tau pv 0) t1 (Printer.tau pv 0) t2) cstrs;

    TMap.map (fun _ -> T_base T_int) unif.u_fvars
 *)




let top_decl env ((x,p,e),(t,s)) =
  let env = { e_vars = env
            ; e_tvar = TSet.empty }
  in

  let new_var v =
    if v = TVar.undef then ~!new_tau else T_var v
  in
  let p = MapT.Fn.pat new_var p in
  let e = MapT.Fn.exp new_var e in

  let _,unif =
    decl false env t (x,p,e)
  in

  let subst = solve unif in

  (x,
   MapT.pat subst p,
   MapT.exp subst e), (t,s)
