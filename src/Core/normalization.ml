(** -------- Normalization -------- **)
(** This pass inserts explicit coercions. **)

open Std
open Ast

(* Create new type and equation variable for
   coercion of exp *)
let add_coerc (_,(_,loc) as exp) =
  E_coer (exp,Type.unit (TVar.create ())),
  (EVar.create (), loc)

(* Add coercions on application arguments and
   existential size introduction *)
let rec exp (exp,loc) = exp' exp, loc
and exp' = function
  | E_app   (_,(E_coer _,_)) as e -> AstMap.exp exp e
  | E_exist (_,(E_coer _,_)) as e -> AstMap.exp exp e
  | E_app   (f,e) -> AstMap.exp exp (E_app   (f, add_coerc e))
  | E_exist (s,e) -> AstMap.exp exp (E_exist (s, add_coerc e))
  | e -> AstMap.exp exp e

(* Declaration level normalization *)
let decl (x,t,e) = x,t,exp e
