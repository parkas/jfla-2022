(** -------- Size checking -------- **)

open Std
open Ast


let rec join loc t1 t2 =
  match t1,t2 with

  | t1,t2 when t1 = t2 -> t1

  (* Pessimist join over refinements *)
  | T_index _, _ | _, T_index _
    | T_size _, _ | _, T_size _
    | T_base T_int, _ | _, T_base T_int -> T_base T_int

  (* Arrow type *)
  | T_arrow (t1,u1), T_arrow (t2,u2) ->
     T_arrow (join loc t2 t1, join loc u1 u2)

  (* Pair type *)
  | T_pair (t1,u1), T_pair (t2,u2) ->
     T_pair (join loc t1 t2, join loc u1 u2)

  (* Errors *)
  | _ -> let v = p_vars () in
         r_error loc "Incompatible types <%a> and <%a>"
           (Printer.tau v 0) t1 (Printer.tau v 0) t2



(* Check t2 <: t1 *)
let rec leq_tau ?(cmp_size=Size.compare) loc t1 t2 =
  let error () =
    let v = p_vars () in
    r_error loc "Incompatible types <%a> and <%a>"
      (Printer.tau v 0) t1 (Printer.tau v 0) t2
  in

  match t1,t2 with

  | t1,t2 when t1 = t2 -> ()

  (* Sizes doesn't matter *)
  | T_index s1, T_index s2
    | T_size s1, T_size s2 ->
     if s1 =@<cmp_size>@ s2 then ()
     else error ()

  (* Integer refinements *)
  | T_base T_int, T_index _
    | T_base T_int, T_size _ -> ()

  (* Arrow type *)
  | T_arrow (t1,u1), T_arrow (t2,u2) ->
     leq_tau ~cmp_size loc t2 t1;
     leq_tau ~cmp_size loc u1 u2

  (* Errors *)
  | _ -> error ()



(* Check p2 <: p1 *)
let leq_pat loc =
  let s_vars () = f_fst #@ (SMap.prop' nop) () in
  let t_vars () = f_snd #@ (TMap.prop' nop) () in
  let rec leq subst p1 p2 =
    match p1, p2 with

    (* Apply substitution to unify polymorphic type variables *)
    | T_mono t1
    , T_mono t2 ->
       t2
       |> MapS.tau s_vars#!subst
       |> MapT.tau t_vars#!subst
       |> leq_tau loc t1

    (* Build substitution of polymorphic type variables *)
    | T_poly (Type v1, p1)
    , T_poly (Type v2, p2) ->
       leq (t_vars #=+ v2 (Type.unit v1) subst) p1 p2

    (* Build substitution of polymorphic size variables *)
    | T_poly (Size v1, p1)
    , T_poly (Size v2, p2) ->
       leq (s_vars #=+ v2 (Size.unit v1) subst) p1 p2

    (* Errors *)
    | _ -> let v = p_vars () in
           r_error loc "Incompatible patterns <%a> and <%a>"
             (Printer.pat v) p1 (Printer.pat v) p2
  in leq (SMap.empty,TMap.empty)




(* Check t2 can be coerced to t1 *)
let coerce loc = function

  (* Any top-level integer refinement may be coerced *)
  | T_size _ | T_index _ as t1 ->
     begin function
       | T_base T_int | T_index _ | T_size _ -> ()
       | t2 -> let v = p_vars () in
               r_error loc "Invalid coercion from <%a> to <%a>"
                 (Printer.tau v 0) t1 (Printer.tau v 0) t2
     end

  (* Any sub-type (regardless sizes) may be coerced *)
  | t1 -> leq_tau ~cmp_size:(fun _ _ -> 0) loc t1


let is_ok fn_t fn_s vars loc elt =
  match TSet.choose_opt (TSet.diff (fn_t elt) (fst vars)) with
  | Some v -> let vars = p_vars () in
              r_error loc "Undefined type variable %s"
                (fst vars v)
  | _ ->
     match SSet.choose_opt (SSet.diff (fn_s elt) (snd vars)) with
     | Some v -> let vars = p_vars () in
                 r_error loc "Undefined size variable %s"
                   (snd vars v)
     | _ -> ()


let eta_ok = is_ok Vars.eta_types Vars.eta_sizes
let tau_ok = is_ok Vars.tau_types Vars.tau_sizes
let pat_ok = is_ok Vars.pat_types Vars.pat_sizes

(* Add quantified type / size variable *)
let add_var = function
  | Type v -> map_fst (TSet.add v)
  | Size v -> map_snd (SSet.add v)

(* Build expression's type pattern *)
let rec exp_pat env vars (exp,(_,loc) as e) =
  match exp with
  | E_Abs (v,e) -> T_poly (v, exp_pat env (add_var v vars) e)
  | E_App (p,e) ->
     begin match p, exp_pat env vars e with
     | Size s, T_poly (Size v,p) -> eta_ok vars loc s; MapS.pat (SMap.singleton v s) p
     | Type t, T_poly (Type v,p) -> tau_ok vars loc t; MapT.pat (TMap.singleton v t) p
     | _, p -> r_error loc "Invalid instanciation of %a" (Printer.pat ~!p_vars) p
     end

  | E_var x -> IMap.find (fst x) env
  | E_fix (x,p,e) -> decl (IMap.add (fst x) p env) vars (x,p,e); p

  (* Expliciting remaining cases *)
  | E_cst _ | E_pair _ | E_coer _ | E_app _ | E_abs _
    | E_let _ | E_exist _ | E_size _ | E_cond _ ->
     T_mono (exp_tau env vars e)


(* Build expression's type *)
and exp_tau env vars (exp,(_,loc) as e) =
  match exp with
  | E_cst c -> c_tau c
  | E_pair (f,s) -> T_pair (exp_tau env vars f, exp_tau env vars s)
  | E_size s -> T_size s
  | E_coer (e,t) -> tau_ok vars loc t; exp_tau env vars e |> coerce loc t; t
  | E_app (f,e) ->
     begin match exp_tau env vars f with
     | T_arrow (t,t') -> exp_tau env vars e |> leq_tau loc t; t'
     | _ -> r_error loc "Function expected"
     end
  | E_abs ((x,t),e) -> tau_ok vars loc t; T_arrow (t, exp_tau (IMap.add (fst x) (T_mono t) env) vars e)
  | E_let ((x,t,e),f) -> decl env vars (x,t,e); exp_tau (IMap.add (fst x) t env) vars f
  | E_exist ((v,s),e) -> exp_tau env vars s |> leq_tau loc (T_base T_int);
                         let tau = exp_tau env (add_var (Size v) vars) e in
                         tau_ok vars loc tau;
                         tau
  | E_cond (c,(t,f)) -> exp_tau env vars c |> leq_tau loc (T_base T_bool);
                        join loc (exp_tau env vars t) (exp_tau env vars f)

  (* Expliciting remaining cases *)
  | E_Abs _ | E_App _ | E_var _ | E_fix _ ->
     match exp_pat env vars  e with
     | T_mono t -> t
     | _ -> r_error loc "Monomorphic type expected"


(* Check expression type *)
and decl env vars (x,p,e) =
  pat_ok vars (exp_loc e) p;
  exp_pat env vars e
  |> leq_pat (snd#.snd e) p


(* Top-level expression type check *)
let check env ((_,_,(_,(_,loc))) as d,(t,s)) =
  if not (TSet.is_empty t) then r_error loc "Unexpected annonymous type variables";
  if not (SSet.is_empty s) then r_error loc "Unexpected annonymous size variables";
  decl env (t,s) d
