open Std
open Fields

include Option

exception Non_none
exception Non_some

(* Functional constructor *)
let some a = Some a

let fold f = function
  | None   -> id
  | Some v -> f v

let fold2 f = function
  | None   -> ign
  | Some v -> function
            | None   -> id
            | Some w -> f v w

let split = function
  | Some (a,b) -> Some a, Some b
  | None -> None, None

let iter2 f a b = fold2 (fun a b () -> f a b) a b ()

let map_fold f a = function
  | None   -> a, None
  | Some v -> let a,r = f a v in a, Some r

let map_fold2 f a = function
  | None   -> fun _ -> a, None
  | Some v -> function
            | None   -> a, None
            | Some w -> let a,r = f a v w in a, Some r



let prop a =
  let add e _ = some e in
  let rem () _ = None in
  F1.( prop a
       |> p_add (a1 add)
       |> p_rem (a1 rem))

let prop' a =
  let add e = function
    | None -> some e
    | _ -> raise Non_none in
  let rem () = function
    | Some _ -> None
    | _ -> raise Non_some in
  F1.( prop a
       |> p_add (a1 add)
       |> p_rem (a1 rem))
