open Fields

(** Extended Set Module

    Provide inter-operability with labels
 **)
module Make =
  functor (Key : Ident.S) ->
  functor (KSet : module type of XSet.Make (Key)) ->
  struct

    include Map.Make (Key)


    (* Belonging checker *)
    let check expct key map =
      match mem key map, expct with
      | true, false -> raise (Key.Not_unique key)
      | false, true -> raise (Key.Not_found key)
      | _ -> ()


    (* Strict version of editor functions *)
    let add' key value map = check false key map; add key value map
    let remove' key map = check true key map; remove key map
    let replace key value map = check true key map; add key value map

    let get_opt map key = find_opt key map
    let get map key = check true key map; find key map
    let edit f key map = check true key map;  add key (f (find key map)) map

    let assoc_default def key map = match find_opt key map with
      | None -> def key
      | Some v -> v

    let pp s f p m = XList.pp s f p (fold (fun i t -> List.cons (i,t)) m [])


    let replace k v m =
      if mem k m then add k v m
      else raise Not_found


    let union_merger f a b = match a b with
      | o,None | None,o -> o
      | Some a , Some b -> Some (f a b)

    let union f =
      merge (fun _ a b -> match a,b with
                          | None, None -> None
                          | o,None | None, o -> o
                          | Some a, Some b -> Some (f a b))

    let find_opt i m = if mem i m then Some (find i m) else None
    let keys m = fold (fun k _ -> KSet.add k) m KSet.empty

    let union' m1 m2 =
      try raise (Key.Not_unique KSet.(choose (inter (keys m1) (keys m2))))
      with Not_found -> union (fun _ _ -> assert false) m1 m2


    let find_default ?default key map =
      match find_opt key map with
      | Some v -> v
      | None -> match default with
                | Some v -> v
                | None -> raise (Key.Not_found key)

    let elt ?default i ()  = F1.(rw (find_default ?default i) (add i) arg)

    let prop  ?default a = F1.(prop a |> p_add (a2 add ) |> p_rem (a1 remove ) |> p_elt (elt ?default))
    let prop' ?default a = F1.(prop a |> p_add (a2 add') |> p_rem (a1 remove') |> p_elt (elt ?default))
  end
