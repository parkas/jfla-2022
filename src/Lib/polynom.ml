open Std



(** Scalar module.
    [add] and [mul] operation are supposed to be
    associative and commutative.
    [mul] operation distributes over [add]
**)
module type Scal = sig
  include Ident.S
  val zero : t
  val one  : t
  val neg : t -> t
  val add : t -> t -> t
  val mul : t -> t -> t
end


(** Polyom functor.
    [S] declares the scalars
    [I] declares the formal variables
    [C] are containers built over [I] to permit inter-operability **)
module Make :
functor (S: Scal) ->
functor (I: Ident.S) ->
functor (C: module type of Containers.Make (I)) ->
sig
  type t

  val zero : t
  val scal : S.t -> t
  val unit : I.t -> t

  val neg    : t -> t
  val add    : t -> t -> t
  val mul    : t -> t -> t

  val ( * ) : t -> t -> t
  val ( + ) : t -> t -> t
  val ( - ) : t -> t -> t

  val compare : t -> t -> int
  val null     : t -> bool
  val not_null : t -> bool
  val to_scalar : t -> S.t option
  val to_unit : t -> I.t option

  val eval : (I.t -> S.t) -> t -> S.t
  val compose : I.t -> t -> t -> t

  (* P -> F * R  where F is unitary *)
  val factorise : t -> t * t

  (* P -> Q - R where Q, R have positive coefficients *)
  val split : t -> t * t

  val vars : t -> C.set
  val print : (I.t -> string) -> (S.t -> string) -> Format.formatter -> t -> unit

end =

  functor (S: Scal) ->
  functor (I: Ident.S) ->
  functor (C: module type of Containers.Make (I)) ->
  struct

    (* Extended scalars with tools *)
    module S = struct
      include S
      let not_null s = s <>@<compare>@ zero
      let null s     = s  =@<compare>@ zero
      let ( + ) = add
      let ( * ) = mul
      let ( - ) a b = a + neg b

      let positif s = s >@<compare>@ zero
      let negatif s = s <@<compare>@ zero
    end


    (* Monomes *)
    module MOrd = struct
      type t = int C.map
      let compare = C.Map.compare (Int.compare)
      exception Not_found of t
      exception Not_unique of t
    end

    module MSet = XSet.Make (MOrd)
    module MMap = XMap.Make (MOrd) (MSet)


    type t = S.t MMap.t
    let compare = MMap.compare S.compare

    let zero = MMap.empty
    let scal s = if S.null s then MMap.empty else MMap.singleton C.Map.empty s
    let unit n = MMap.singleton (C.Map.singleton n 1) S.one

    let neg = MMap.map S.neg

    let add p1 p2 =
      MMap.union S.add p1 p2
      |> MMap.filter (fun _ -> S.not_null)

    let mul p1 p2 =
      MMap.fold (fun m1 s1 ->
          MMap.fold (fun m2 s2 ->
              MMap.singleton (C.Map.union (+) m1 m2) (S.mul s1 s2)
              |> add) p2) p1 zero


    let not_null s = s <>@<compare>@ zero
    let null s     = s  =@<compare>@ zero
    let ( + ) = add
    let ( * ) = mul
    let ( - ) a b = a + neg b



    let eval valuation polynom =
      MMap.fold (fun m s ->
          C.Map.fold (fun v o ->
              (S.mul (valuation v))#*o) m s
          |> S.add) polynom S.zero


    let compose n p p2 =
      MMap.fold (fun m s ->
          let o = C.Map.find_default ~default:0 n m in
          add (MMap.singleton (C.Map.remove n m) s |> (mul p)#*o) ) p2 zero

    let split p =
      p
      |> MMap.partition (fun _ s -> s >@<S.compare>@ S.zero)
      |> map_snd (MMap.map S.neg)

    let factorise p =
      let common _ o1 o2 =
        let (~:) = Option.value ~default:0 in
        match min ~:o1 ~:o2 with
        | 0 -> None
        | n -> Some n
      in
      let f =
        C.Map.empty
        |> MMap.fold (fun k _ -> C.Map.union max k) p
        |> MMap.fold (fun k _ -> C.Map.merge common k) p
      in
      MMap.singleton f S.one,
      MMap.fold (fun m s -> MMap.add (C.Map.merge (fun _ -> function
                                          | None -> assert false
                                          | Some o -> fun i -> match Int.sub o (Option.value ~default:0 i) with
                                                               | 0 -> None
                                                               | n -> Some n) m f) s) p MMap.empty



    let print pi ps p t =
      if MMap.is_empty t then Format.fprintf p "0"
      else
        MMap.fold (fun m s f ->
            let scal = C.Map.is_empty m in
            let f =
              if      s =@<S.compare>@ S.one       && not scal then (if f then Format.fprintf p "+"; false)
              else if s =@<S.compare>@ S.neg S.one && not scal then (Format.fprintf p "-"; false)
              else (Format.fprintf p "%s%s" (if S.positif s && f then "+" else "") (ps s); true)
            in
            C.Map.fold (fun i o f -> if o = 1 then Format.fprintf p "%s%s" (if f then "*" else "") (pi i)
                                     else Format.fprintf p "%s%s^%d" (if f then "*" else "")(pi i) o; true)
              m f |> ignore;
            true)
          t false
        |> ignore

    let vars p = MSet.fold (fun m -> C.Set.union (C.Map.keys m)) (MMap.keys p) C.Set.empty

    let to_scalar p =
      if C.Set.is_empty (vars p)
      then MMap.find_opt C.Map.empty p |> Option.value ~default:S.zero |> Option.some
      else None

    let to_unit p =
      let vars = vars p in
      if C.Set.cardinal vars = 1 &&
           C.Set.fold (fun i -> add (unit i)) vars zero =@<compare>@ p
      then Some (C.Set.choose vars) else None
  end

