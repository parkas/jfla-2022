open Std
open Fields

(** Extended list module

**)
include List

exception Not_unique


(* Functionnal constructors *)
let empty = []
let cons x l = x :: l

let is_empty = function [] -> true | _ -> false

let cut n l =
  (function l,a::r -> a::l,r
           | _ -> raise (Failure "out of range")) #* n ([],l) |> map_fst List.rev
let take n l = fst (cut n l)
let drop n l = snd (cut n l)


(* Memory optimized extraction of last element *)
let last =
  let rec (-) e = function
    | [] -> e
    | a::l -> a - l
  in function
  | [] -> raise Not_found
  | a::l -> a - l


(* Lexicographic ordering of lists *)
let compare cmp list1 list2 =
  let rec order = function
    |  [] ,  []  ->  0
    |  [] ,   _  -> -1
    |  _  ,  []  -> +1
    | a::l, b::m -> cmp a b =<> order (l,m)
  in order (list1,list2)


(* Helper for pretty printing *)
let pp s f =
  Format.(pp_print_list ~pp_sep:(fun p () -> fprintf p s)) f

(* Convenient fold_left to use as other folds *)
let fold reduce list init =
  fold_left (fun acc elt -> reduce elt acc) init list

(* Convenient fold_left2 to use as other folds *)
let fold2 reduce list1 list2 init =
  fold_left2 (fun acc elt1 elt2 -> reduce elt1 elt2 acc)
    init list1 list2

(* Mapping list with an accumulator *)
let map_fold map_fold init list =
  let elem elt (acc,mlist) =
    let acc,elt = map_fold acc elt in
    acc, elt::mlist
  in
  (init,[])
  |> fold elem list
  |> map_snd (List.rev)

(* Mapping list with an accumulator *)
let map_fold2 map_fold init list1 list2 =
  let elem elt1 elt2 (acc,mlist) =
    let acc,elt = map_fold acc elt1 elt2 in
    acc, elt::mlist
  in
  (init,[])
  |> fold2 elem list1 list2
  |> map_snd (List.rev)

(* Filter-map: keep values mapped to 'Some _' *)
let map_opt map list =
  fold (fun elt ->
      match map elt with
      | None -> id
      | Some elt -> cons elt ) list []
  |> rev

(* Filter-map: keep values mapped to 'Some _' *)
let map_opt2 map list1 list2 =
  fold2 (fun elt1 elt2 ->
      match map elt1 elt2 with
      | None -> id
      | Some elt -> cons elt ) list1 list2 []
  |> rev

(* Remove the common start of a list pair *)
let rec differ ?(cmp=(=)) = function
  | a::l, b::m when cmp a b -> differ ~cmp (l,m)
  | p -> p

(* Remove the common start of a list pair *)
let rec common ?(cmp=(=)) = function
  | a::l, b::m when cmp a b -> a :: (common ~cmp (l,m))
  | p -> []


(* Belonging checker *)
let check ?(cmp=(=)) expct elt list =
  match List.exists (cmp elt) list, expct with
  | true, false -> raise Not_unique
  | false, true -> raise Not_found
  | _ -> ()

(* Editor functions *)
let insert ?cmp elt list = elt :: list
let rec remove ?(cmp=(=)) elt = function
  |   []   -> []
  | hd::tl -> if cmp elt hd then tl else hd :: remove ~cmp elt tl

(* Strict version of editor functions *)
let insert' ?cmp elt list = check ?cmp false elt list; insert ?cmp elt list
let remove' ?cmp elt list = check ?cmp true  elt list; remove ?cmp elt list

(* Element accessors *)
let get_nth n l = nth l n
let rec edit n f l = match n with
  | 0 -> f (hd l) :: tl l
  | n -> hd l :: edit (n-1) f (tl l)

(* Field for nth element *)
let f_nth n () = F1.(re (get_nth n) (edit n) arg)

(* Field properties for list *)
let prop  ?cmp a = F1.(prop a |> p_add (a1 (insert  ?cmp)) |> p_rem (a1 (remove  ?cmp)) |> p_elt f_nth)
let prop' ?cmp a = F1.(prop a |> p_add (a1 (insert' ?cmp)) |> p_rem (a1 (remove' ?cmp)) |> p_elt f_nth)


(** Association lists **)
module Assoc = struct
  let empty = []

  (* Convenient fold_left to use as other folds *)
  let fold f a =
    fold (fun (k,e) -> f k e) a

  let mem ?(cmp=(=)) key =
    List.exists (fst #. (cmp key))

  (* New binding insersion *)
  let insert ?(cmp=(=)) key ?before value =
    let rec iter = function
      | [] -> if before = None then (key,value) :: []
              else raise Not_found
      | hd::tl -> if cmp (fst hd) key then raise Not_unique
                  else if match before with
                          | None      -> false
                          | Some bkey -> cmp (fst hd) bkey
                  then if List.exists (fst #. (cmp key)) tl then raise Not_unique
                       else (key, value) :: hd :: tl
                  else hd :: iter tl
    in iter


  let rec remove ?(cmp=(=)) key = function
    |   []   -> raise Not_found
    | hd::tl -> if cmp key (fst hd) then tl else hd :: remove ~cmp key tl

  let get ?(cmp=(=)) key = find (fst#.(cmp key))
  let edit ?(cmp=(=)) key fct =
    let rec iter = function
      |   []   -> raise Not_found
      | hd::tl -> if cmp key (fst hd) then fct hd :: tl else hd :: iter tl
    in iter
  let _i_mapper ?cmp edit key ?before value data = edit (insert ?cmp key ?before value) data

  (* Field for key associated element *)
  let elt ?cmp key () = F1.(re (get ?cmp key) (edit ?cmp key) arg)
  let elt ?cmp key () = (elt ?cmp key) #-- f_fst ()

  (* Properties (strict) for association list *)
  let prop ?cmp a b = F1.( prop (pair a b)
                           |> p_add _i_mapper
                           |> p_rem (a1 (remove ?cmp))
                           |> p_elt (elt ?cmp) )
end
