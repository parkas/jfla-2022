(** -------- Concrete Syntax Tree -------- **)

open Std
include Syntax.Cst


(* General (non recusrive) variable unification (no vars occurs in...) *)
type 'a unif =
  | Val of 'a
  | Var of 'a unif option ref

(* Forces either Val _  or Var (ref None) *)
let rec deref = function
  | Val a -> Val a
  | Var r -> match !r with
             | None -> Var r
             | Some u -> match u with
                         | Val a -> Val a
                         | Var s -> Option.iter Option.some#.((:=) r) !s; deref u

(* Unify two unifiers given unification of values f *)
let unif f u1 u2 =
  match deref u1, deref u2 with
  | Var v1, Var v2 -> let v = Var (ref None) in
                      v1 := Some v; v2:= Some v
  | u, Var v | Var v, u -> v := Some u
  | Val v1, Val v2 -> f v1 v2

let var () = Var (ref None)



(* Valid size operations *)
type etaop = etaop' loc
and etaop' =
  | S_add | S_sub | S_mul

(* Sizes *)
type eta = eta' loc
and eta' =
  | S_int of int loc
  | S_var of var
  | S_bop of etaop * eta opt pair

(* Data types *)
type tau = tau' loc
and tau' =
  | T_var of var
  | T_bool
  | T_int
  | T_size of eta opt
  | T_index of eta opt
  | T_array of eta opt * tau opt

(* Function types *)
type ftau = ftau' loc
and ftau' = eta opt list opt * tau opt list opt pair

(* Type schemes *)
type pat = pat' loc
and pat' =
  | T_mono of ftau
  | T_poly of (var list, var list) param * pat


(* Supported binary operators *)
type binop = binop' loc
and binop' =
  | O_eq  | O_ne                  (* Generic comparison   *)
  | O_ge  | O_le  | O_gt  | O_lt  (* Numerical comparison *)
  | O_and | O_ior | O_xor         (* Boolean operators    *)
  | O_add | O_sub | O_mul | O_div (* Numerical operators  *)
  | O_rem                         (* Interger operator    *)

(* Supported unary operators *)
type unop = unop' loc
and unop' =
  | O_not
  | O_neg

(* Type of application/abstraction *)
type app =
  | A_arg
  | A_size
  | A_index

(* Iterators *)
type iter =
  | I_map
  | I_fold
  | I_scan

(* Expressions *)
type exp = exp' loc
and exp' =
  | E_impl  of int unif pair * exp
  | E_op    of var
  | E_var   of var
  | E_int   of int loc
  | E_bool  of bool loc
  | E_fop   of binop
  | E_unop  of unop  * exp
  | E_binop of binop * exp pair
  | E_coer  of exp * ftau
  | E_pow   of eta opt list * exp
  | E_app   of exp * exp opt list * app
  | E_abs   of intro list * exp
  | E_let   of decl * exp
  | E_exist of (var * exp) * exp
  | E_size  of eta opt
  | E_iter  of (iter loc * int unif pair) * exp
  | E_cond  of exp * exp pair
  | E_err

and decl = decl' * location opt'
and decl' =
  | D_val of var *              pat opt * exp
  | D_fun of var * intro list * tau opt * exp

and intro = intro' loc
and intro' =
  | I_svar  of var list
  | I_tvar  of var list
  | I_var   of (var opt * tau opt) list
  | I_size  of (var opt * eta opt) list
  | I_index of (var opt * eta opt) list

and top_decl = decl loc * bool loc opt'


