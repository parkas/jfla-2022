
open Std
open Cst




let intro_pat recf intros tau =
  let vars,sizes,args,tau =
    List.fold_right (function
        | I_svar _, _ -> id
        | I_tvar _, _ -> id
        | I_var l, loc -> fun (v,s,a,t) -> v,s,(List.map snd l @ fst a, snd a <-> loc),t
        | I_size l, loc ->
           fun (v,s,a,t) ->
           List.filter_map (function
               | (Some v,_), (None,_) -> Some v
               | _ -> None) l @ v,
           (List.map (function
                | (Some v,_),(None,_) -> Some (S_var v, snd v), snd v
                | _,eta      -> eta) l, loc)
           ,a,t
        | I_index l, _ ->
           fun (v,s,a,t) ->
           v,s,a,List.fold_right (fun (_,s) t -> let l = snd s <-> snd t in
                                                 Some (T_array (s,t), l),l) l t
      ) intros ([],([],snd tau),([],snd tau),tau)
  in
  let loc = snd sizes <-> snd tau in
  if Option.is_none recf then None, loc else
    Some (T_poly (Size vars, (T_mono (((Some (fst sizes), snd sizes),
                                       ((Some (fst args), snd args),
                                        (Some [tau], snd tau))),loc), loc)), loc), loc

let tau_decl = function
  | None, _ -> id
  | Some t, l -> fun e -> let x = "_x",l in
                          let t = ((Some [] ,l),
                                   ((Some [],l),
                                    (Some [Some t,l],l))),l in
                          E_let ((D_val (x,(Some (T_mono t,l),l),e), None), (E_var x,l)),
                          l <-> snd e

let rec exp (exp,loc) = exp' exp, loc
and exp' = function
  | E_impl _ -> assert false
  | E_op v -> E_op v
  | E_var v -> E_var v
  | E_int i -> E_int i
  | E_bool b -> E_bool b
  | E_fop o -> E_fop o
  | E_unop (o,a) -> E_unop (o, exp a)
  | E_binop (o,p) -> E_binop (o, Pair.map exp p)
  | E_coer (e,t) -> E_coer (exp e,t)
  | E_pow (s,e) -> E_pow (s,exp e)
  | E_app (e,a,k) -> E_app (exp e, List.map (map_fst (Option.map exp)) a, k)
  | E_abs (l,e) -> List.fold_right (fun i e -> E_abs ([i],e), snd i) l (exp e) |> fst
  | E_let (d,e) -> E_let (decl d, exp e)
  | E_exist ((v,s),e) -> E_exist ((v,exp s), exp e)
  | E_size s -> E_size s
  | E_iter (i,e) -> E_iter (i, exp e)
  | E_cond (c,(t,f)) -> E_cond (exp c, (exp t, exp f))
  | E_err -> E_err

and decl (decl,recf) = decl' recf decl, recf
and decl' recf = function
  | D_val (v,  p,e) -> D_val (v, p, exp e)
  | D_fun (v,i,t,e) -> D_val (v, intro_pat recf i t, exp (E_abs (i,tau_decl t e), snd e))


let implementation decls =
  List.map (map_fst (map_fst decl)) decls
