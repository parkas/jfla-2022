%{
    open Std
    open Cst

    let position p =
      { l_file = p.Lexing.pos_fname;
        l_lbeg = p.pos_lnum;
        l_lend = p.pos_lnum;
        l_cbeg = p.pos_bol;
        l_cend = p.pos_bol; }

    let (!?) = Option.value ~default:dummy_loc
    let (!:) = fst
    let (!) = snd

    (* Empty size list for const arrays adds 1 dim *)
    let (!+) a = match !:a with
      | [] -> [None, !a]
      | sz -> sz

    (* Hack for size parameter applications *)
    let (!-) a = List.map (map_fst (Option.map (fun s -> E_size (Some s, !s),!s))) (!:a)

    let var () = Var (ref None), Var (ref None)

    let pipe f e = E_app (f,[Some e, !e], A_arg), !e <-> !f
    let f_pipe l f =
      let n = "_x",l in
      let e = f (E_var n, l) in
      E_abs ([I_var [(Some n,l), (None,l)],l],e), l <-> !e
%}

(**** Tokens ****)
%token <bool Std.loc> TEST
%token <Cst.tau> BTYPE
%token <Std.location> LET REC FUN IN ARROW EQUAL SIZE TYPE IF THEN ELSE
%token <Std.location> LPAREN RPAREN LBRACK RBRACK LANGLE RANGLE LCURLY RCURLY
%token <Std.location> COLON DOT COMMA
%token <Std.location> UNDERSCORE
%token <int    Std.loc> INT
%token <bool   Std.loc> BOOL
%token <string Std.loc> IDENT LETTER BUILTIN
%token <Cst.iter Std.loc> ITER
%token <Std.location> PIPE BARBAR AMPAMP LT GT LE GE NE NOT HAT
%token <Std.location> PLUS MINUS STAR SLASH PERCENT
%token EOF

(**** Precedence ****)
%nonassoc pipe
%left PIPE
%left BARBAR
%left AMPAMP
%left HAT
%nonassoc EQUAL NE LT GT LE GE
%left PLUS MINUS
%left STAR SLASH PERCENT
%nonassoc NOT
%left RBRACK
%left LANGLE LBRACK LPAREN UNDERSCORE
%nonassoc ITER


%start <Cst.top_decl list> program
%%


(**** Utils ****)
%inline paren (item): l=LPAREN i=item r=RPAREN { i, l <-> r } //  ( . )
%inline brack (item): l=LBRACK i=item r=RBRACK { i, l <-> r } //  [ . ]
%inline curly (item): l=LCURLY i=item r=RCURLY { i, l <-> r } //  { . }
%inline angle (item): l=LANGLE i=item r=RANGLE { i, l <-> r } // << . >>
%inline s_ang (item): l=LT     i=item r=GT     { i, l <-> r } //  < . >

%inline e_list (sep,item): l=separated_list         (sep,item) { l }
%inline n_list (sep,item): l=separated_nonempty_list(sep,item) { l }


%inline opt_e (item): // Inlined explicit placeholder
  | l=UNDERSCORE { None  ,  l }
  | i=item       { Some i, !i }

%inline opt_i (item): // Implicit optional value
  | /* empty */   { None, position $startpos }
  | o=opt_e(item) { o }

%inline opt_s (SEP, item): // Separated optional value
  | /* empty */         { None, position $startpos }
  | l=SEP o=opt_e(item) { !:o, l <-> !o }

(**** Sizes *****)
eta:
  | n=INT                              { S_int n, !n }
  | v=IDENT                            { S_var v, !v }
  | v=LETTER                           { S_var v, !v }
  | a=opt_e(eta) o=etaop b=opt_e(eta)  { S_bop (o,(a,b)), !a <-> !b }
  | e=paren(eta)                       { map_fst fst e }

%inline etaop:
  | l=PLUS  { S_add, l }
  | l=MINUS { S_sub, l }
  | l=STAR  { S_mul, l }

(**** Types *****)
tau:
  | b=BTYPE                            { b }
  | v=IDENT                            { T_var v    , !v }
  | v=LETTER                           { T_var v    , !v }
  | s=brack(opt_e(eta))                { T_index !:s, !s }
  | s=brack(opt_i(eta)) t=opt_e(tau)   { T_array (!:s,t), !s <-> !t }
  | t=paren(tau)                       { map_fst fst t }


dtau:
  | t=opt_e(tau) ARROW l=UNDERSCORE { ([t], (None,l)), !t <-> l }
  | t=opt_e(tau) ARROW u=dtau       { map_fst (List.cons t) !:u, !t <-> !u }
  | t=tau                           { ([], (Some [Some t, !t], !t)), !t }
ftau:
  | d=dtau { let (i,o),l = d in ((None, l), ((Some i,l),o)), !d}
  | s=s_ang(e_list(COMMA,opt_e(eta))) l=ARROW d=dtau { let (i,o),l = d in
                                                       (map_fst (Option.some) s,
                                                        ((Some i,l),o)), !s <-> !d }

sigma:
  | t=ftau                      { T_mono t          , !t       }
  | l=SIZE v=IDENT* DOT s=sigma { T_poly (Size v, s), l <-> !s }
  | l=TYPE v=IDENT* DOT s=sigma { T_poly (Type v, s), l <-> !s }

(**** Constants ****)
%inline binop:
  | EQUAL   { O_eq , $1 }
  | NE      { O_ne , $1 }
  | LT      { O_lt , $1 }
  | GT      { O_gt , $1 }
  | LE      { O_le , $1 }
  | GE      { O_ge , $1 }
  | HAT     { O_xor, $1 }
  | BARBAR  { O_ior, $1 }
  | AMPAMP  { O_and, $1 }
  | PLUS    { O_add, $1 }
  | MINUS   { O_sub, $1 }
  | STAR    { O_mul, $1 }
  | SLASH   { O_div, $1 }
  | PERCENT { O_rem, $1 }

%inline unop:
  | NOT             { O_not, $1 }

(**** Expressions ****)
pipes:
  | f=exp   %prec PIPE { pipe f }
  | f=pipes PIPE g=exp { f <|> pipe g }
exp:
  | x=BUILTIN                 { E_op x              , !x        }
  | l=DOT                     { E_err               , l         }
  | x=IDENT                   { E_var x             , !x        }
  | b=BOOL                    { E_bool b            , !b        }
  | i=INT                     { E_int i             , !i        }
  | o=paren(binop)            { E_fop !:o           , !o        }
  | o=unop e=exp              { E_unop (o,e)        , !o <-> !e }
  | a=exp o=binop b=exp       { E_binop (o,(a,b))   , !a <-> !b }
  | s=curly(opt_e(eta))       { E_size !:s          , !s        }
  | i=ITER e=exp              { E_iter ((i,~!var),e), !i <-> !e }
  | s=brack(e_list(COMMA,opt_e(eta))) e=exp { E_pow (!+s,e)        , !s <-> !e }
  | f=exp a=brack(n_list(COMMA,opt_e(exp))) { E_app (f,!:a,A_index), !f <-> !a }
  | f=exp a=angle(n_list(COMMA,opt_e(eta))) { E_app (f,!-a,A_size ), !f <-> !a }
  | f=exp e=paren(n_list(COMMA,opt_e(exp))) { E_app (f,!:e,A_arg  ), !f <-> !e }
  | l=PIPE f=pipes %prec pipe { f_pipe l f }
  | e=exp PIPE f=exp          { pipe f e   }
  | e=paren(coerce)           { map_fst fst e }
  | e=paren(exp_)             { map_fst fst e }
exp_:
  | e=exp { e }
  | l=LET d=decl IN e=exp_                      { E_let (!:d,e)    , l <-> !e }
  | l=LET SIZE v=IDENT EQUAL s=exp_ IN e=exp_   { E_exist ((v,s),e), l <-> !e }
  | l=FUN a=intro+ ARROW e=exp_                 { E_abs (a,e)      , l <-> !e }
  | l=IF c=exp_ THEN t=exp_ ELSE f=exp_         { E_cond (c,(t,f)) , l <-> !f }

coerce: e=exp COLON t=ftau { E_coer (e,t), !e <-> !t }

decl: r=REC? d=decl2 { (!:d,r), !?r <-> !d }
decl2:
  | x=IDENT p=opt_s(COLON,sigma) EQUAL e=exp_        { D_val (x,  p,e), !x <-> !e }
  | x=IDENT l=intro+ t=opt_s(COLON,tau) EQUAL e=exp_ { D_fun (x,l,t,e), !x <-> !e }

intro:
  | x=opt_e(IDENT) { I_var [x,(None,position $endpos)], !x  }
  | l=paren(i_svar) { I_svar !:l, !l }
  | l=paren(i_tvar) { I_tvar !:l, !l }
  | l=brack(n_list(COMMA,i_intro)) { I_index !:l, !l }
  | l=angle(n_list(COMMA,s_intro)) { I_size !:l, !l }
  | l=paren(n_list(COMMA,a_intro)) { I_var !:l, !l }
i_intro: x=opt_e(IDENT) s=opt_s(LT   ,eta) { x, s }
s_intro: x=opt_e(IDENT) s=opt_s(EQUAL,eta) { x, s }
a_intro: x=opt_e(IDENT) s=opt_s(COLON,tau) { x, s }
i_svar: SIZE l=IDENT* { l }
i_tvar: TYPE l=IDENT* { l }

(**** Value declaration ****)
let_decl: l=LET d=decl   { !:d, l <-> !d }

top_decl: d=let_decl t=TEST? { d,t }

(**** Entry point *****)
program: d=top_decl* EOF { d }
