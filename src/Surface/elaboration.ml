

open Std
open Cst

module C = Syntax.Cst

let opt f = map_fst (Option.map f)
let some (o,l) = Some (o,l),l

(* Size elaboration *)
let rec eta (eta,loc) = eta' eta, loc
and eta' = function
  | S_int i -> C.S_int i
  | S_var v -> C.S_var v
  | S_bop ((o,l),(a,b)) ->
     match o with
     | S_add -> C.S_add (opt eta a, opt eta b)
     | S_mul -> C.S_mul (opt eta a, opt eta b)
     | S_sub -> C.S_add (opt eta a,
                         some (C.S_mul (some (C.S_int (-1,l),l),
                                        opt eta b), l <-> snd b))

(* Extract size type from optional annotation *)
let size = function
  | Some s, l -> some (C.T_size (some (eta s)), l)
  | None  , l -> some (C.T_size (None, l),l)

(* Extract index type from optional annotation *)
let index = function
  | Some s, l -> some (C.T_index (some (eta s)),l)
  | None  , l -> some (C.T_index (None, l),l)



(* Type elaboration *)
let rec tau (tau,loc) = tau' tau, loc
and tau' = function
  | T_var v -> C.T_var v
  | T_bool  -> C.T_bool
  | T_int   -> C.T_int
  | T_size s  -> C.T_size (opt eta s)
  | T_index s -> C.T_index (opt eta s)
  | T_array (s,t) -> C.T_arrow (index s, opt tau t)

(* Function type elaboration *)
let ftau ((etas,(itaus,otaus)), loc) =
  let (=>) d c = some (C.T_arrow (d,c), snd d <-> snd c) in
  let otau = match otaus with
    | None, l -> None, l
    | Some [otau], _ -> otau
    | _ -> failwith "Multiple return types unsupported"
  in
  opt tau otau
  |> List.fold_right (opt tau)#.(=>) (Option.value ~default:[] (fst itaus))
  |> List.fold_right      size#.(=>) (Option.value ~default:[] (fst etas))

let coerc tau exp =
  C.E_coer (exp,tau), snd tau <-> snd exp

let abs tau (var,ann) exp =
  C.E_abs ((var,tau ann), exp), snd var <-> snd exp
let app arg fn =
  C.E_app (fn,arg), snd fn <-> snd arg

let unop (o,l) =
  let open Core.Builtins in
  (C.E_var (fst#.fst
              (match o with
               | O_neg -> neg
               | O_not -> not
              ), l)), l

let binop (o,l) =
  let open Core.Builtins in
  (C.E_var (fst#.fst
              (match o with
               | O_eq -> eq
               | O_ne -> ne
               | O_lt -> lt
               | O_gt -> gt
               | O_le -> le
               | O_ge -> ge
               | O_add -> add
               | O_sub -> sub
               | O_mul -> mul
               | O_div -> div
               | O_rem -> rem
               | O_and -> bnd
               | O_ior -> ior
               | O_xor -> xor
              ), l)), l

let iter ((i,l),(a,b)) =
  let extr a = match deref a with
    | Val n -> n
    | _ -> r_error l "Undefined iterator arity"
  in
  let a = extr a in
  let b = extr b in
  assert (b = 1);
  let open Core.Builtins in
  (C.E_var (fst#.fst
              (match i with
               | I_map  -> List.nth i_map  (a  -1)
               | I_fold -> List.nth i_fold (a-b-1)
               | I_scan -> List.nth i_scan (a-b-1)
              ), l)), l

let extract_size = function
  | C.E_var v -> C.E_size (some (C.S_var v,snd v))
  | C.E_size e -> C.E_size e
  | _ -> failwith "Bad value expression"

let arg kind (exp,loc) =
  match kind with
  | A_arg -> exp, loc
  | A_size  -> extract_size exp, loc
  | A_index -> coerc (Some (T_index (None,loc), loc), loc) (exp,loc)

let rec pat (pat,loc) = pat' loc pat
and pat' loc = function
  | T_mono t -> C.T_mono (ftau t), loc
  | T_poly (Size l, p) -> List.fold_right (fun v p -> C.T_poly (Size v, p),loc) l (pat p)
  | T_poly (Type l, p) -> List.fold_right (fun v p -> C.T_poly (Type v, p),loc) l (pat p)

let to_pat = function
  | Some p, _ -> p
  | None,l -> C.T_mono (None, l), l

let unused = function
  | (Some s, _), a ->   s   , a
  | (None  , l), a -> ("",l), a


let rec exp (exp,loc) = exp' exp, loc
and exp' : _ -> C.exp' = function
  | E_impl ((ex,en),e) -> i_app ex en e |> fst
  | E_op v -> E_op v
  | E_var v -> E_var v
  | E_int i -> E_int i
  | E_bool b -> E_bool b
  | E_unop (o,e) -> E_app (unop o, exp e)
  | E_binop (o,(a,b)) -> E_app ((E_app (binop o, exp a), snd a <-> snd o), exp b)
  | E_size s -> E_size (opt eta s)
  | E_coer (e,t) -> E_coer (exp e, ftau t)
  | E_app (f,l,k) -> p_app f l k |> fst
  | E_abs (d,e) -> exp e |> List.fold_right intro d |> fst
  | E_exist ((i,s),e) -> E_exist ((i,exp s), exp e)
  | E_let (d,e) -> E_let (decl d,exp e)
  | E_cond (c,p) -> E_cond (exp c, Pair.map exp p)
  | E_err -> E_err
  | E_pow (s,e) -> intro (I_index (List.map (fun s -> (None, snd s), s) s),
                          List.fold snd#.(<->) s dummy_loc) (exp e) |> fst
  | E_fop o -> binop o |> fst
  | E_iter (i,e) -> E_app (iter i, exp e)

and i_app ex en e =
  let l = snd e in
  match deref ex, deref en with
  | Val n, Val p when n = p -> exp e
  | Val 0, Val n -> exp (E_app (e, (List.cons (Some (E_size (None,l),l),l))#*n [], A_size), snd e)
  | Val n, Val p -> r_error l "Static arity mismatch: expected %d encontered %d" n p
  | Var _, Var _ -> exp e|> ignore ; i_app ex en e
  | _ -> unif (fun _ _ -> assert false) ex en;
         i_app ex en e

and p_app f l k =
  let exp, vars, _ =
    List.fold (fun (a,l) (e,v,n) ->
        match a with
        | Some a -> exp#.(arg k)#.app a e, v, n
        | None -> let a = ~%"_%d" n,l in
                  app (E_var a,l) e, (some a,(None,l))::v, n+1
      ) l (exp f,[],1)
  in
  intro (I_var (List.rev vars), List.fold fst#.snd#.(<->) vars dummy_loc) exp

and decl (d,r) =
  let x,p,e = decl' d in
  if Option.is_none r then x,p,e
  else x,p,(E_fix (x,p,e), snd e)

and decl' = function
  | D_val (x,p,e) -> x,opt pat p |> to_pat,exp e |> Option.fold p_vars (fst p)
  | D_fun (x,a,t,e) ->
     x,to_pat (None,snd x),
     e
     |> exp
     |> (if Option.is_some (fst t) then coerc (opt tau t) else id)
     |> List.fold_right intro a

and p_vars = function
  | T_mono _    , _ -> id
  | T_poly (Size l,p), _ -> p_vars p <|> List.fold_right (fun v e -> C.E_Abs (Size v, e), snd v <-> snd e) l
  | T_poly (Type l,p), _ -> p_vars p <|> List.fold_right (fun v e -> C.E_Abs (Type v, e), snd v <-> snd e) l


and intro = function
  | I_svar  l,_ -> List.fold_right (fun v e -> C.E_Abs (Size v, e), snd v <-> snd e) l
  | I_tvar  l,_ -> List.fold_right (fun v e -> C.E_Abs (Type v, e), snd v <-> snd e) l
  | I_var   l,_ -> List.fold_right (abs id) (List.map unused#.(map_snd (opt tau)) l)
  | I_size  l,_ -> List.fold_right (abs size) (List.map (fun (v,(_,l)) -> v,(Some (S_var v,l),l)) (List.map unused l))
                   <|> List.fold_right (fun (v,_) e -> C.E_Abs (Size v, e), snd v <-> snd e) (List.map unused l)
  | I_index l,_ -> List.fold_right (abs index) (List.map unused l)


let implementation decls = List.map (map_fst (map_fst decl)) decls

