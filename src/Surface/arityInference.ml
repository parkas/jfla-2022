
open Std
open Cst


module M = Map.Make (String)






type arity = int unif * int unif pair

let u_arity kind loc ex en =
  if ex <> en then r_error loc "%s arity mismatch: expected %d, encountered %d" kind ex en

let unify loc (ex_s,(ex_i,ex_o)) (en_s,(en_i,en_o)) =
  unif (u_arity "Size"   loc) ex_s en_s;
  unif (u_arity "Input"  loc) ex_i en_i;
  unif (u_arity "Output" loc) ex_o en_o





let data o = Val 0, (Val 0, o)
let func i o = Val 0, (i, o)
let schm s i o = s, (i, o)
let value = data (Val 1)
let any () = ~!var,(~!var,~!var)

let add_var arity (var,loc) =
  M.add var arity

let get_var env (var,loc) =
  try M.find var env
  with Not_found -> r_error loc "Undefined variable %s" var


let build_arity n p = function
  | A_arg   -> let var_o = ~!var in
               (Val 0, (Val n, var_o)),
               (Val 0, (Val p, var_o))
  | A_size  -> let var_i = ~!var in
               let var_o = ~!var in
               (Val n, (var_i, var_o)),
               (Val p, (var_i, var_o))
  | A_index -> value, value

let data_arity = function
  | Core.Ast.T_var _ -> 1
  | Core.Ast.T_base _ -> 1
  | Core.Ast.T_arrow (T_index _,_) -> 1
  | _ -> failwith "Non canonical type"

let rec func_arity = function
  | Core.Ast.T_arrow (T_index _,_) as t -> let o = data_arity t in 0, Val o
  | Core.Ast.T_arrow (T_size _,_) -> failwith "Non canonical type"
  | Core.Ast.T_arrow (_, t) -> let i,o = func_arity t in i+1, o
  | t -> let o = data_arity t in 0, Val o

let rec type_arity = function
  | Core.Ast.T_arrow (T_size _,t) -> let s,p = type_arity t in s+1, p
  | t -> let i,o = func_arity t in 0, (Val i, o)

let rec schm_arity = function
  | Core.Ast.T_poly (_,p) -> schm_arity p
  | Core.Ast.T_mono t -> let s,p = type_arity t in Val s, p

let builtin v =
  try List.find fst#.fst#.((=) (fst v)) Core.Builtins.builtins |> snd#.schm_arity
  with Not_found -> r_error (snd v) "Undefined builtin %s" (fst v)

let rec exp env (sa,da) (exp,loc) =
  let exp,(sa',da') = exp' env loc exp in
  unify loc (Val 0, da) (Val 0, da');
  match deref sa, deref sa' with
  | Val n, Val m when n = m -> exp, loc
  | _ -> E_impl ((sa, sa'), (exp,loc)), loc


and exp' env loc = function
  | E_impl _ -> assert false
  | E_op v -> E_op v, builtin v
  | E_var v  -> E_var v, get_var env v
  | E_int i  -> E_int i, value
  | E_bool b -> E_bool b, value

  | E_fop o -> E_fop o, (Val 0, (Val 2, Val 1))
  | E_unop (o,e) -> E_unop (o, exp env value e), value
  | E_binop (o,p) -> E_binop (o, Pair.map (exp env value) p), value

  | E_coer (e,t) -> let a = Val 0, (Val 0, Val 1) in
                    E_coer (exp env a e, t), a

  | E_pow (s,e) -> E_pow (s,exp env value e), value

  | E_abs (l,e) ->
     let gen i =
       let a = ~!any in
       E_abs ([i], exp env a e), a
     in
     let abs i v k =
       let f,b = build_arity (List.length v) 0 k in
       E_abs ([i], exp (List.fold fst#.fst#.(Option.fold (add_var value)) v env) b e), f
     in
     begin match l with
     | [I_svar  v, l] -> gen (I_svar  v, l)
     | [I_tvar  v, l] -> gen (I_tvar  v, l)
     | [I_var   v, l] -> abs (I_var   v, l) v A_arg
     | [I_size  v, l] -> abs (I_size  v, l) v A_size
     | [I_index v, l] -> abs (I_index v, l) v A_index
     | _ -> failwith "Normalization pass should be run prior arity inference"
     end


  | E_app (e,a,k) ->
     let f,r = build_arity (List.length a)
                 (List.length (List.filter fst#.Option.is_none a)) k
     in
     E_app (exp env f e,
            List.map (map_fst (Option.map (exp env value))) a, k), r


  | E_let (d,e) -> let d, env = decl env d in
                   let a = Val 0, (Val 0, Val 1) in
                   E_let (d, exp env a e), a

  | E_exist ((v,s),e) -> let a = ~!any in
                         E_exist ((v,exp env value s),
                                  exp env a e), a
  | E_size s -> E_size s, value

  | E_iter (i,e) -> let var_i = ~!var in
                    let var_o = ~!var in
                    E_iter ((fst i, (var_i, var_o)), exp env (Val 0, (var_i, var_o)) e), (Val 1, (var_i, var_o))

  (* First order conditionnal. To relax ? *)
  | E_cond (e,p) -> let a = Val 0, (Val 0, ~!var) in
                    E_cond (exp env value e, Pair.map (exp env a) p), a

  | E_err -> E_err, ~!any


(* TODO: ignoring annotations *)
and decl env (decl, rec_f) =
  let a = ~! any in
  let v,p,e = match decl with
    | D_val (v,p,e) -> v,p,e
    | _ -> failwith "Run normalization pass first"
  in
  let env' = add_var a v env in

  (D_val (v,p,exp (if Option.is_some rec_f then env' else env) a e), rec_f), env'


let decl env ((d,t),l) =
  let d,env = decl env d in
  env, ((d,t),l)

let implementation decls =
  List.map_fold decl M.empty decls
  |> snd
