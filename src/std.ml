(* The module Std contains shared definitions used across multiple
   parts of the compiler *)



open Lib
include Std
include Fields


let cpt () =
  let n = ref 0 in
  let get () =
    n := !n + 1; !n
  in get



(** To convert __POS__ to it's start location **)
let pos_loc (f,l,c,_) = f,l,c



module List = struct
  include XList
  include Iterator (XList)
end

let list  a = List.prop  a
let list' a = List.prop' a

module Option = struct
  include XOption
  include Iterator (XOption)
end


let option  a = Option.prop  a
let option' a = Option.prop' a

module I = Containers.Make (Ident.SIdent)


module Int = struct
  include Int
  exception Not_found of t
  exception Not_unique of t
end

module IC = Containers.Make (Int)
module ISet = IC.Set
module IMap = IC.Map
type iset = ISet.t
type 'a imap = 'a IMap.t
let iset  () = ISet.prop  ()
let iset' () = ISet.prop' ()
let imap  a = IMap.prop  a
let imap' a = IMap.prop' a


(**---- Source locations ----------------**)

(* Source location *)
type location =
  { l_file : string;
    l_lbeg : int;
    l_lend : int;
    l_cbeg : int;
    l_cend : int; }

(* Invalid location *)
let dummy_loc =
  { l_file = "";
    l_lbeg = -1;
    l_lend = -1;
    l_cbeg = -1;
    l_cend = -1; }

(* Location merger: locations must target the same file *)
let extend li lf =
  if      li = dummy_loc then lf
  else if lf = dummy_loc then li
  else begin
      assert (li.l_file = lf.l_file);
      if li.l_lend = lf.l_lend
      then { li with l_cend = lf.l_cend }
      else { li with l_cend = lf.l_cend;
                     l_lend = lf.l_lend(*;
                     l_ebol = lf.l_ebol*) }
    end


(* Location merger operator *)
let (<->) li lf = extend li lf

(* Mocation OCaml's style pretty printer *)
let output_location p l =
  let open Format in
  if l.l_lbeg = l.l_lend
  then fprintf p "File \"%s\", line %d, characters %d-%d"
         l.l_file l.l_lbeg l.l_cbeg l.l_cend
  else fprintf p "File \"%s\", lines %d-%d, characters %d-%d"
         l.l_file l.l_lbeg l.l_lend l.l_cbeg l.l_cend

(* Source related message *)
let print_loc prefix title content p loc =
  Format.fprintf p "@.%s%a:@.%s@[<v 2>%s: %a@]"
    prefix output_location loc prefix title content ()

(* Main located messages *)
let warning content = print_loc "" "Warning" content Format.std_formatter
let error   content = print_loc "" "Error"   content Format.std_formatter
let note    content = print_loc "  " "Note"  content Format.std_formatter

(* Simple string messages *)
let warning_m message = warning (fun p () -> Format.fprintf p "%s" message)
let error_m   message = error   (fun p () -> Format.fprintf p "%s" message)
let note_m    message = note    (fun p () -> Format.fprintf p "%s" message)


exception Error

let _SEP_IN = true
let _SEP_OUT = true

let pp_loc cnt prefix title fmter loc format =
  let open Format in
  kfprintf (fun fmter ->
      kfprintf (fun fmter ->
          kfprintf cnt fmter (if _SEP_OUT then "@]@." else "@]")
        ) fmter format)
    fmter (if _SEP_IN then "@.%s%a:@.%s@[<v 2>%s: " else "%s%a:@.%s@[<v 2>%s: ")
    prefix output_location loc prefix title

(* Print error message and raise exception *)
let p_warning loc = pp_loc ignore "" "Warning" Format.std_formatter loc
let p_error   loc = pp_loc ignore "" "Error"   Format.std_formatter loc
let r_error   loc = pp_loc (fun _ -> raise Error) "" "Error"   Format.std_formatter loc


(**---- Core definitions ----------------**)

type ident = int

(* Name handling *)
module Names : sig
  val to_string : int -> string
  val create : string -> int
  val rename : int -> string -> unit
end =
  struct
    let names = Hashtbl.create 997
    let () = Hashtbl.add names 0 ""
    let id_nb = ref 0

    let create name =
      id_nb := !id_nb + 1;
      Hashtbl.add names !id_nb name;
      !id_nb

    let rename = Hashtbl.replace names

    let to_string i = match Hashtbl.find_opt names i with
      | Some s -> ~%"%s" (if s = "" then "_" else s)
      | None   -> ~%"_%d" i
  end

let p_ident p i = Format.fprintf p "%s" (Names.to_string i)



module PList = CC (Pair) (List)

(* Convenient aliases *)
type 'a loc = 'a * location
type 'a opt = 'a option


module TOrder (C : sig type t
                       val cmp : t -> t -> int
                   end) =
  struct
    include C
    let eq a b = cmp a b = 0
    let lt a b = cmp a b < 0
    let gt a b = cmp a b > 0
    let le a b = cmp a b <= 0
    let ge a b = cmp a b >= 0
    let ne a b = cmp a b <> 0
    let min a b = if a < b then a else b
    let max a b = if a > b then a else b
  end




(* Typed integer variables *)
module Vars (R:sig val shft : int
                   val pref : string end) :
sig
  open Lib
  module I : Ident.S
  include module type of Containers.Make (I)

  val ids : unit -> (t -> string -> unit) * (t -> string)
  val undef : t
  val create : unit -> t
end =

  struct
    (* Identifier mapping *)
    let names ?(shft=0) pref =
      let names = ref IMap.empty in

      let rec new_id ?(nb=1) id =
        let idn = if nb < 2 then id else id^(string_of_int nb) in
        if IMap.exists (fun _ n -> idn = n) !names then new_id ~nb:(nb+1) id
        else idn
      in

      let insert i n = names := IMap.add i (new_id n) !names in
      let get i =
        if i = 0 then "?" else
          match IMap.find_opt i !names with
          | Some n -> n
          | None -> let n = new_id (Format.sprintf "%s%c" pref "abcdefghijklmnopqrstuvwxyz".[(IMap.cardinal !names+shft) mod 26]) in
                    insert i n; n
      in
      insert, get




    module I = Int
    module C = IC
    include I
    include C

    open R
    let ids () = names ~shft pref
    let create = cpt ()
    let undef = 0
  end




(** Monad signature and related operators **)
module Monad = struct

  (** Monad signature **)
  module type S = sig
    type 'a t
    val return : 'a -> 'a t
    val bind : 'a t -> ('a -> 'b t) -> 'b t
  end

  (** Operators (let*, >>=, ...) introduction **)
  module Intro (M: S) = struct
    include M
    let (>>=) e f = bind e f
    let (let*) e f = bind e f
    let (let+) e f = bind e f#.return
    let (and*) a b = let* a = a in
                     let+ b = b in a,b
    let (and+) a b = (and*) a b

    let rec mapM f = function
      | [] -> return []
      | a::l -> let+ a = f a
                and+ l = mapM f l in a::l

    let rec iterM f = function
        | [] -> return ()
        | a::l -> let+ () = f a
                  and+ () = iterM f l in ()
  end
end



(** State monad and utilities **)
module StateMonad = struct

  (** State monad signature **)
  module type S = sig
    include Monad.S
    type s
    type effect = unit t
    val effect : (s -> s) -> effect
    val read : (s -> 'a) -> 'a t
    val init : s -> 'a t -> 'a * s
    val process : s -> unit t -> s
  end

  (** Canonical state monad implementation **)
  module Make (S: sig type t end) = struct
    type s = S.t
    type 'a t = s -> 'a * s
    type effect = unit t
    let effect f res = (), f res
    let read f s = f s, s
    let init s c = c s
    let process s c = init s c |> snd
    let return a res = a,res
    let bind a f res =
      let a,res = a res in
      f a res
  end

  (** Operators (let*, >>=, ...) introduction **)
  module Intro (M: S) = struct
    include M
    include Monad.Intro (M)

    let (let<>) (b,e) f =
      let+ () = b
      and+ r = f ()
      and+ () = e in r

    let (let<->) pe = pe

  end



  module Pair (Fst: S) (Snd: S) = struct
    module M = Make (struct type t = Fst.s * Snd.s end)
(*    module Intr = struct*)
      include Intro (M)

      let e_fst eff = effect (map_fst (fun s -> let (),s = Fst.init s eff in s))
      let e_snd eff = effect (map_snd (fun s -> let (),s = Snd.init s eff in s))

      let b_fst blk = Pair.map e_fst blk
      let b_snd blk = Pair.map e_snd blk

(*    end*)
  end


  module Empty  = struct
    type s = unit
    type 'a t = 'a
    type effect = unit t
    let effect _ = ()
    let read f = f ()
    let init _ a = a, ()
    let process _ = id
    let return a = a
    let bind a f = f a
  end

end


