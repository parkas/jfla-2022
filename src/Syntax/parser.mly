%{
    open Std
    open Cst

    let (!:) = fst
    let (!) = snd

    (* The only syntactical sugar a - b = a + (-1)*b *)
    let minus a l b =
      S_add (a, (Some (S_mul ((Some (S_int (-1,l),
                                     l),l), b),
                       l <-> !b), l <-> !b))
%}

(**** Tokens ****)
%token <bool Std.loc> TEST
%token <Cst.tau> BTYPE
%token <Std.location> LET FIX FUN IN ARROW EQUAL SIZE TYPE CASE THEN ELSE
%token <Std.location> LPAREN RPAREN LBRACK RBRACK LANGLE RANGLE LCURLY RCURLY COLON DOT
%token <Std.location> UNDERSCORE
%token <int    Std.loc> INT
%token <bool   Std.loc> BOOL
%token <string Std.loc> IDENT LETTER QUESTION BUILTIN
%token <Std.location> PLUS STAR MINUS
%token EOF

(**** Precedence ****)
%left PLUS MINUS
%left STAR
%left LBRACK LPAREN LANGLE INT BOOL IDENT BUILTIN UNDERSCORE app

%start <Cst.top_decl list> program
%%


(**** Utils ****)
%inline paren (item): l=LPAREN i=item r=RPAREN { i, l <-> r } // ( . )
%inline brack (item): l=LBRACK i=item r=RBRACK { i, l <-> r } // [ . ]
%inline curly (item): l=LCURLY i=item r=RCURLY { i, l <-> r } // { . }
%inline angle (item): l=LANGLE i=item r=RANGLE { i, l <-> r } // < . >

%inline opt (item): // Inlined explicit placeholder
  | l=UNDERSCORE { None  ,  l }
  | i=item       { Some i, !i }


(**** Sizes *****)
eta:
  | n=INT               { S_int n, !n }
  | v=IDENT             { S_var v, !v }
  | v=LETTER            { S_var v, !v }
  | a=opt(eta) PLUS b=opt(eta)    { S_add (a,b), !a <-> !b }
  | a=opt(eta) STAR b=opt(eta)    { S_mul (a,b), !a <-> !b }
  | a=opt(eta) l=MINUS b=opt(eta) { minus a l b, !a <-> !b }
  | e=paren(eta)        { map_fst fst e }

(**** Types *****)
tau:
  | b=BTYPE                      { b }
  | v=IDENT                      { T_var v    , !v }
  | v=LETTER                     { T_var v    , !v }
  | l=QUESTION                   { T_var l    , !l }
  | s=angle(opt(eta))            { T_size !:s , !s }
  | s=brack(opt(eta))            { T_index !:s, !s }
  | s=brack(opt(eta)) t=opt(tau) { T_arrow ((Some (T_index !:s, !s),!s),t), !s <-> !t }
  | t=paren(tau_)                { map_fst fst t }
tau_:
  | t=tau                        { t }
  | t=opt(tau) ARROW u=opt(tau_) { T_arrow (t, u), !t <-> !u }

sigma:
  | t=opt(tau_)                { T_mono t            , !t       }
  | l=SIZE v=IDENT DOT s=sigma { T_poly (Size v, s), l <-> !s }
  | l=TYPE v=IDENT DOT s=sigma { T_poly (Type v, s), l <-> !s }

(**** Expressions ****)
exp:
  | x=BUILTIN                 { E_op x       , !x        }
  | x=IDENT                   { E_var x      , !x        }
  | b=BOOL                    { E_bool b     , !b        }
  | i=INT                     { E_int i      , !i        }
  | s=angle(opt(eta))         { E_size !:s   , !s        }
  | f=exp e=exp     %prec app { E_app (f,e)  , !f <-> !e }
  | e=exp t=brack(inst)       { E_App (!:t,e), !e <-> !t }
  | e=paren(coerce)           { map_fst fst e }
  | e=paren(exp_)             { map_fst fst e }
exp_:
  | e=exp { e }
  | l=FIX d=decl                                { E_fix  !:d       , l <-> !d }
  | l=LET d=decl IN e=exp_                      { E_let (!:d,e)    , l <-> !e }
  | l=LET SIZE v=IDENT EQUAL s=exp_ IN e=exp_   { E_exist ((v,s),e), l <-> !e }
  | l=FUN x=IDENT COLON t=opt(tau) ARROW e=exp_ { E_abs ((x,t),e)  , l <-> !e }
  | l=SIZE v=IDENT ARROW e=exp_                 { E_Abs (Size v,e) , l <-> !e }
  | l=TYPE v=IDENT ARROW e=exp_                 { E_Abs (Type v,e) , l <-> !e }
  | l=CASE c=exp_ THEN t=exp_ ELSE f=exp_       { E_cond (c,(t,f)) , l <-> !f }
inst:
  | l=SIZE s=opt(eta) { Size s }
  | l=TYPE t=opt(tau) { Type t }
coerce: e=exp COLON t=opt(tau_) { E_coer (e,t), !e <-> !t }

decl: x=IDENT COLON t=sigma EQUAL e=exp_ { (x,t,e), !x <-> !e }

(**** Value declaration ****)
let_decl: l=LET d=decl   { !:d, l <-> !d }

top_decl: d=let_decl t=TEST? { d,t }

(**** Entry point *****)
program: d=top_decl* EOF { d }
