(** -------- Concrete Syntax Tree -------- **)

open Std

type ident = string
type var = ident loc

type 'a opt = 'a opt' loc
and 'a opt' = 'a option

type ('t,'s) param =
  | Type of 't
  | Size of 's

type eta = eta' loc
and eta' =
  | S_int of int loc
  | S_var of var
  | S_add of eta opt pair
  | S_mul of eta opt pair

type tau = tau' loc
and tau' =
  | T_var of var
  | T_bool
  | T_int
  | T_size of eta opt
  | T_index of eta opt
  | T_arrow of tau opt pair

type pat = pat' loc
and pat' =
  | T_mono of tau opt
  | T_poly of (var, var) param * pat

type exp = exp' loc
and exp' =
  | E_op    of var
  | E_var   of var
  | E_int   of int loc
  | E_bool  of bool loc
  | E_coer  of exp * tau opt
  | E_app   of exp * exp
  | E_abs   of (var * tau opt) * exp
  | E_App   of (tau opt, eta opt) param * exp
  | E_Abs   of (var, var) param * exp
  | E_fix   of decl
  | E_let   of decl * exp
  | E_exist of (var * exp) * exp
  | E_size  of eta opt
  | E_cond  of exp * exp pair
  | E_err

and decl = var * pat * exp

and top_decl = decl loc * bool loc opt'
