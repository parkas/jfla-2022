{
  open Lexing
  open Parser
  open Std

  type lexical_error =
    | Illegal_character
    | Unterminated_comment
    | Bad_char_constant
    | Unterminated_string;;

  exception Lexical_error of lexical_error * int * int;;


  let init filename channel =
    let lb = Lexing.from_channel channel in
    lb.lex_curr_p <- {lb.lex_curr_p with pos_fname = filename; pos_lnum = 1};
    lb

  let position lb =
    let p = Lexing.lexeme_start_p lb in
    { l_file = p.Lexing.pos_fname;
      l_lbeg = p.Lexing.pos_lnum;
      l_lend = p.Lexing.pos_lnum;
      l_cbeg = Lexing.lexeme_start lb - p.Lexing.pos_bol;
      l_cend = Lexing.lexeme_end lb   - p.Lexing.pos_bol; }

  let keyword_table = Hashtbl.create 149
  let () =
    List.iter (fun (str,tok) -> Hashtbl.add keyword_table str (fun lb -> tok (position lb)))
      [ "let",      (fun p -> LET     p)
      ; "fix",      (fun p -> FIX     p)
      ; "fun",      (fun p -> FUN     p)
      ; "in",       (fun p -> IN      p)
      ; "size",     (fun p -> SIZE    p)
      ; "type",     (fun p -> TYPE    p)
      ; "case",     (fun p -> CASE    p)
      ; "then",     (fun p -> THEN    p)
      ; "else",     (fun p -> ELSE    p)
      ; "int",      (fun p -> BTYPE   (T_int,  p))
      ; "bool",     (fun p -> BTYPE   (T_bool, p))
      ; "true",     (fun p -> BOOL    (true, p))
      ; "false",    (fun p -> BOOL    (false,p))
      ]
}


rule token = parse
  | ("\010" | "\013" | "\013\010") { new_line lexbuf; token lexbuf}
  | [' ' '\009' '\012'] +   { token lexbuf }
  | "("             { LPAREN      (position lexbuf) }
  | ")"             { RPAREN      (position lexbuf) }
  | "["             { LBRACK      (position lexbuf) }
  | "]"             { RBRACK      (position lexbuf) }
  | "{"             { LCURLY      (position lexbuf) }
  | "}"             { RCURLY      (position lexbuf) }
  | "<"             { LANGLE      (position lexbuf) }
  | ">"             { RANGLE      (position lexbuf) }
  | ":"             { COLON       (position lexbuf) }
  | "."             { DOT         (position lexbuf) }
  | "="             { EQUAL       (position lexbuf) }
  | "_"             { UNDERSCORE  (position lexbuf) }
  | "->"            { ARROW       (position lexbuf) }

  | "+"             { PLUS        (position lexbuf) }
  | "-"             { MINUS       (position lexbuf) }
  | "*"             { STAR        (position lexbuf) }

  | "'"['a'-'z'] { LETTER (Lexing.lexeme lexbuf, position lexbuf)}
  | "?"          { QUESTION ("?",position lexbuf) }

  | ("$"['a'-'z' '_' 'A'-'Z'](['A'-'Z' 'a'-'z' ''' '0'-'9' '_']) * as id)
       { BUILTIN (id, position lexbuf) }

  | (['a'-'z' '_' 'A'-'Z'](['A'-'Z' 'a'-'z' ''' '0'-'9' '_']) * as id)
      { let s = Lexing.lexeme lexbuf in
        try Hashtbl.find keyword_table s lexbuf
        with Not_found -> IDENT (id, position lexbuf) }

  | ['0'-'9']+
  | '0' ['x' 'X'] ['0'-'9' 'A'-'F' 'a'-'f']+
  | '0' ['o' 'O'] ['0'-'7']+
  | '0' ['b' 'B'] ['0'-'1']+
      { INT (int_of_string(Lexing.lexeme lexbuf), position lexbuf) }

  | "/*" { comment_block lexbuf }
  | "//" ' '* "FAIL" { let t = TEST (false, position lexbuf) in end_line lexbuf; t }
  | "//" ' '* "SUCC" { let t = TEST (true , position lexbuf) in end_line lexbuf; t }
  | "//" { comment_line lexbuf }

  | eof            {EOF}
  | _              {raise (Lexical_error (Illegal_character,
					  Lexing.lexeme_start lexbuf,
					  Lexing.lexeme_end lexbuf))}

and comment_line = parse
  | ("\010" | "\013" | "\013\010") { new_line lexbuf; token lexbuf }
  | _                              { comment_line lexbuf }

and comment_block = parse
  | "*/"                           { token lexbuf }
  | ("\010" | "\013" | "\013\010") { new_line lexbuf; comment_block lexbuf }
  | _                              { comment_block lexbuf }
  | eof
      { raise (Lexical_error
                (Unterminated_comment, 0, Lexing.lexeme_start lexbuf)) }

and end_line = parse
  | ("\010" | "\013" | "\013\010" ) { new_line lexbuf }
  | _                               { end_line lexbuf }
