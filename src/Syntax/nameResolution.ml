(** -------- Name resolution -------- **)

(** This pass transforms CST into AST by giving unique
    names to variables and introducing fresh names for
    place-holders.
**)

open Std
open Cst
module A = Core.Ast

module M = Map.Make (String)

type env =
  { expr_vars : Std.ident M.t
  ; size_vars : (string, A.SVar.t) Hashtbl.t
  ; type_vars : (string, A.TVar.t) Hashtbl.t }

let get_evar env (x,l) =
  try M.find x env.expr_vars,l
  with Not_found -> r_error l "Undefined variable %s" x

let get_svar env (x,l) =
  try Hashtbl.find env.size_vars x
  with Not_found ->
    if x.[0] <> '\'' then r_error l "Undefined size variable %s" x
    else let v = A.SVar.create () in
             Hashtbl.add env.size_vars x v; v

let get_tvar env (x,l) =
  if x = "?" then A.TVar.undef else
    try Hashtbl.find env.type_vars x
    with Not_found ->
      if x.[0] <> '\'' then r_error l "Undefined type variable %s" x
      else let v = A.TVar.create () in
           Hashtbl.add env.type_vars x v; v

let (+=) env (x,l) =
  let n = Names.create x in
  (n,l),
  { env with expr_vars = M.add x n env.expr_vars }

let add_size (i,l) env =
  if Hashtbl.mem env.size_vars i then r_error l "Aleardy defined size variable %s" i
  else let v = A.SVar.create () in
       Hashtbl.add env.size_vars i v; v

let new_size () = A.Size.unit (A.SVar.create ())
let new_type () = A.Type.unit (A.TVar.create ())

let opt f d env = function
  | None,_ -> d ()
  | Some i,_ -> f env i


let scope htbl v b f a =
  let () = Hashtbl.add htbl v b in
  let r = f a in
  let () = Hashtbl.remove htbl v in
  r

let rec eta env =
  fst#.(function
        | S_int n -> A.Size.scal (fst n)
        | S_var x -> A.Size.unit (get_svar env x)
        | S_add (s1,s2) -> A.Size.(o_eta env s1 + o_eta env s2)
        | S_mul (s1,s2) -> A.Size.(o_eta env s1 * o_eta env s2)
  )
and o_eta env = opt eta new_size env

let rec tau env =
  fst#.(function
        | T_int -> A.T_base T_int
        | T_bool -> A.T_base T_bool
        | T_var x -> A.T_var (get_tvar env x)
        | T_size s -> A.T_size (o_eta env s)
        | T_index s -> A.T_index (o_eta env s)
        | T_arrow (t,u) -> A.T_arrow (o_tau env t, o_tau env u)
  )
and o_tau env = opt tau new_type env

let sigma env =
  let rec sigma = function

    | T_mono t, _ ->
       let t = o_tau env t in
       (A.T_mono t,M.(empty, empty)),
       A.Vars.(tau_types t, tau_sizes t)

    | T_poly (Type (v,l),s),_ ->
       let nv = A.TVar.create () in
       let (s,(tb,sb)),(tv,sv) = scope env.type_vars v nv sigma s in
       if A.TSet.mem nv tv |> not then p_warning l "Generalizing over unused type variable %s" v;
       (A.T_poly (Type nv,s), M.(add v nv tb, sb)), (A.TSet.remove nv tv, sv)

    | T_poly (Size (v,l),s),_ ->
       let nv = A.SVar.create () in
       let (s,(tb,sb)),(tv,sv) = scope env.size_vars v nv sigma s in
       if A.SSet.mem nv sv |> not then p_warning l "Generalizing over unused size variable %s" v;
       (A.T_poly (Size nv,s), M.(tb, add v nv sb)), (tv, A.SSet.remove nv sv)

  in sigma <|> fst
let o_sigma env = opt sigma (fun () -> A.T_mono (new_type ()), M.(empty,empty)) env

let rec exp env (exp,loc) = exp' env exp, (A.EVar.create (), loc)
and exp' env = function
  | E_op x   -> A.E_var (get_evar env x)
  | E_var x  -> A.E_var (get_evar env x)
  | E_int n  -> A.E_cst (C_int (fst n))
  | E_bool b -> A.E_cst (C_bool (fst b))
  | E_size s -> A.E_size (o_eta env s)
  | E_coer (e,t) -> A.E_coer (exp env e, o_tau env t)
  | E_app (f,e)  -> A.E_app  (exp env f, exp env e)
  | E_abs ((x,t),e) -> let x,env = env += x in A.E_abs ((x,o_tau env t), exp env e)
  | E_App (Type t,e) -> A.E_App (Type (o_tau env t), exp env e)
  | E_App (Size s,e) -> A.E_App (Size (o_eta env s), exp env e)

  | E_Abs (Type v,e) ->
     let nv = A.TVar.create () in
     let e = scope env.type_vars (fst v) nv (exp env) e in
     A.E_Abs (Type nv,e)

  | E_Abs (Size v,e) ->
     let nv = A.SVar.create () in
     let e = scope env.size_vars (fst v) nv (exp env) e in
     A.E_Abs (Size nv,e)

  | E_exist ((i,s),e) ->
     let nv = A.SVar.create () in
     let e = scope env.size_vars (fst i) nv (exp env) e in
     A.E_exist ((nv,exp env s), e)

  | E_cond (c,(t,f)) ->
     A.E_cond (exp env c, (exp env t, exp env f))

  | E_fix  d    -> let d,env = decl true  env d in A.E_fix  d
  | E_let (d,e) -> let d,env = decl false env d in A.E_let (d,exp env e)

  | E_err -> failwith "Not implemented yet"

and decl rec_f env (x,t,e) =
  let x,env' = env += x in
  let t,(tb,sb) = sigma env t in
  (x,t, exp (if rec_f then env' else env) e), env'

let htbl_fold_unique fn tbl init =
  let add i v m = assert (not (M.mem i m)); M.add i v m in
  let m = Hashtbl.fold add tbl M.empty in
  M.fold fn m init

let decl env (d,t) =
  Hashtbl.clear env.type_vars;
  Hashtbl.clear env.size_vars;
  let (x,p,e),env = decl false env (fst d) in

(* (* One by one abstraction: prevent solving of remaining constraints *)
  let e =
    e
    |> htbl_fold_unique (fun _ v e -> A.E_Abs (Size v,e),(A.EVar.create (), snd#.snd e)) env.size_vars
    |> htbl_fold_unique (fun _ v e -> A.E_Abs (Type v,e),(A.EVar.create (), snd#.snd e)) env.type_vars
  in
  env, (((x,p,e),
         (A.TSet.empty,
          A.SSet.empty)), t)
*)

  let d,env = decl false env (fst d) in
  env, ((d, (htbl_fold_unique ign#.((#.)A.TSet.add') env.type_vars A.TSet.empty,
             htbl_fold_unique ign#.((#.)A.SSet.add') env.size_vars A.SSet.empty)), t)


let implementation l =
  let init = List.fold fst#.(uncurry M.add) Core.Builtins.builtins M.empty in
  List.map_fold decl { expr_vars = init;
                       size_vars = Hashtbl.create 127;
                       type_vars = Hashtbl.create 127 } l
  |> snd

(*
let f = let g (x:'a) (y:'b) = x=y in g 0 1
let f (type a) (type b) = let g (x:a) (y:b) = x=y in g 0 1
*)
